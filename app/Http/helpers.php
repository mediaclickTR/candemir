<?php

use Mediapress\Foundation\UserAgent\UserAgent;

define("HOME_SMID",1);
define("APPLICATION_FORM",11);
define("EBULLETIN_SMID",13);
define("SERVICE_SMID",3);
define("ABOUT_SMID_2",3);
define("SECURITY_SMID",4);
define("NEWS_SMID",9);
define("CONTACT_SMID",12);
define("AREAS_WE_SERVE",15);
define("CLEANING_SMID",16);

function shortText($text,$limit = 50)
{
    $text = strip_tags($text);
    if (strlen($text) > $limit) {
        $text = mb_substr($text, 0, $limit) . "...";
    }
    return $text;
}

function detailUrlRedirect($link)
{
    $url = \Mediapress\Modules\MPCore\Models\Url::find($link);
    if ($url) {
        return $url->url;
    }
    return "javascript:void(0);";
}

function getPageUrlById($id)
{
    $page = Page::with('detail.extras', 'extras')->find($id);
    if ($page){
        return url($page->detail->url);
    }
    return 'javascript:void(0);';
}

function getMonths()
{
    $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December',
    );

    return $months;
}

function getSitemapById($id)
{
    $data = \Mediapress\Modules\Content\Models\Sitemap::with('detail.extras', 'extras')->find($id);
    if ($data){
        return $data;
    }
    return null;
}

function getUserAgent()
{
    $ua = new UserAgent();
    return $ua->getDevice();
}

function getPageDetailByID($page_id)
{
    $page = \Mediapress\Modules\Content\Models\Page::where("id", $page_id)->with(['detail', 'extras'])->first();
    if ($page) {
        return $page;
    } else {
        return null;
    }
}

function getCategoryDetailByID($id)
{
    $category = \Mediapress\Modules\Content\Models\Category::where("id", $id)->with(['detail', 'extras'])->first();
    if ($category) {
        return $category;
    } else {
        return null;
    }
}

function isImageObject($object)
{
    if (is_a($object, 'Illuminate\Database\Eloquent\Collection')) {
        return true;
    }
    return false;
}

function tofloat($num)
{
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
    );
}

function convertDate($date,$lang)
{
    if($lang == "tr"){
        $date = explode(" ",translateDate("j M Y",$date));
        $date = ['day'=>$date[0],'month'=>$date[1], 'year'=>$date[2]];
    }else{
        $date = strtotime($date);
        $date = ['day'=>date("d",$date),'month'=>date("M",$date), 'year'=>date("Y",$date)];
    }
    return $date;
}

function parseDate($date, $name = false, $lang = "tr"){
    if ($date == null)
        return array("year" => null,"day" => null, "month"=>null);

    $ex = explode("-", $date);
    $arr = [];
    $arr["year"] = $ex[0];
    $arr["day"] = $ex[2];
    if ($name == "true")
        $arr["month"] = getMonthName($ex[1], $lang);
    else
        $arr["month"] = $ex[1];
    return $arr;
}

function getMonthName($month, $lang)
{
    $tr = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"];
    $en = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    if (strtolower($lang) == "tr")
        return $tr[$month - 1];
    else
        return $en[$month - 1];
}


