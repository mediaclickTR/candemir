<?php

namespace App\Modules\Content\Website1\AllBuilder\Renderables\Home;


use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Sitemap extends BuilderRenderable
{
    public
    function defaultContents()
    {
        extract($this->params);
        return [
            [
                "type" => "form",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "method" => "post",
                            "action" => route("Content.sitemaps.mainUpdate", ["id" => $sitemap->id]),
                        ]
                    ],
                    "collectable_as"=>["sitemapform", "form"]
                ],
                "contents" =>
                /*EDITOR*/
[
    0 => [
        'type' => 'steptabs',
        'contents' => [
            0 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'detailtabs',
                        'contents' => [
                            0 => [
                                'type' => 'tab',
                                'contents' => [
                                    0 => [
                                        'type' => 'inputwithlabel',
                                        'options' => [
                                            'html' => [
                                                'tag' => 'input',
                                                'attributes' => [
                                                    'class' => 'detail-name',
                                                    'name' => 'detail->name',
                                                    'type' => 'text',
                                                    'value' => '<print>detail->name</print>',
                                                    'style' => '',
                                                ],
                                            ],
                                            'title' => 'Sayfa Yapısı Başlığı',
                                            'rules' => '',
                                        ],
                                    ],
                                    1 => [
                                        'type' => 'accordion',
                                        'contents' => [
                                            0 => [
                                                'type' => 'accordionpane',
                                                'contents' => [
                                                    0 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->about_title',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->about_title</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Hakkımızda Başlık ',
                                                        ],
                                                    ],
                                                    1 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->about_title_2',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->about_title_2</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Hakkımızda Başlık 2.Satır',
                                                        ],
                                                    ],
                                                    2 => [
                                                        'type' => 'textareawithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'textarea',
                                                                'attributes' => [
                                                                    'class' => 'form-control',
                                                                    'name' => 'detail->extras->about_detail',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Hakkımızda Detay Metni',
                                                            'value' => '<print>detail->about_detail</print>',
                                                        ],
                                                    ],
                                                    3 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->button_title',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->button_title</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Buton Metni',
                                                        ],
                                                    ],
                                                    4 => [
                                                        'type' => 'selectwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'select',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->button_url',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Hakkımızda Buton Bağlantısı',
                                                            'multiple' => '0',
                                                            'value' => '<print>detail->button_url</print>',
                                                            'show_default_option' => '0',
                                                            'additional_content' => 'merge',
                                                            'default_value' => '',
                                                            'default_text' => '---Seçiniz---',
                                                        ],
                                                        'data' => [
                                                            'values' => [
                                                                'type' => 'ds:Content->Urls',
                                                            ],
                                                            'stacks' => [
                                                                'scripts' => '<script></script>',
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => 'card accordion-pane',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Hakkımızda',
                                                    'initial_status' => 'closed',
                                                ],
                                            ],
                                            1 => [
                                                'type' => 'accordionpane',
                                                'contents' => [
                                                    0 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_title',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->hr_title</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Başlık',
                                                        ],
                                                    ],
                                                    1 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_subtitle',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->hr_subtitle</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Başlık 2',
                                                        ],
                                                    ],
                                                    2 => [
                                                        'type' => 'textareawithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'textarea',
                                                                'attributes' => [
                                                                    'class' => 'form-control',
                                                                    'name' => 'detail->extras->hr_detail',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Detay',
                                                            'value' => '<print>detail->hr_detail</print>',
                                                        ],
                                                    ],
                                                    3 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_title_1',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->hr_title_1</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Bağlantı Başlık 1',
                                                        ],
                                                    ],
                                                    4 => [
                                                        'type' => 'selectwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'select',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_url_1',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Bağlantı',
                                                            'multiple' => '0',
                                                            'value' => '<print>detail->hr_url_1</print>',
                                                            'show_default_option' => '0',
                                                            'additional_content' => 'merge',
                                                            'default_value' => '',
                                                            'default_text' => '---Seçiniz---',
                                                        ],
                                                        'data' => [
                                                            'values' => [
                                                                'type' => 'ds:Content->Urls',
                                                            ],
                                                            'stacks' => [
                                                                'scripts' => '<script></script>',
                                                            ],
                                                        ],
                                                    ],
                                                    5 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_title_2',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->hr_title_2</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Bağlantı Başlık 2',
                                                        ],
                                                    ],
                                                    6 => [
                                                        'type' => 'selectwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'select',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_url_2',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Bağlantı',
                                                            'multiple' => '0',
                                                            'value' => '<print>detail->hr_url_2</print>',
                                                            'show_default_option' => '0',
                                                            'additional_content' => 'merge',
                                                            'default_value' => '',
                                                            'default_text' => '---Seçiniz---',
                                                        ],
                                                        'data' => [
                                                            'values' => [
                                                                'type' => 'ds:Content->Urls',
                                                            ],
                                                            'stacks' => [
                                                                'scripts' => '<script></script>',
                                                            ],
                                                        ],
                                                    ],
                                                    7 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_title_3',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->hr_title_3</print>',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'İnsan Kaynakları Bağlantı Başlık 3',
                                                        ],
                                                    ],
                                                    8 => [
                                                        'type' => 'selectwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'select',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->hr_url_3',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Bağlantı',
                                                            'multiple' => '0',
                                                            'value' => '<print>detail->hr_url_3</print>',
                                                            'show_default_option' => '0',
                                                            'additional_content' => 'merge',
                                                            'default_value' => '',
                                                            'default_text' => '---Seçiniz---',
                                                        ],
                                                        'data' => [
                                                            'values' => [
                                                                'type' => 'ds:Content->Urls',
                                                            ],
                                                            'stacks' => [
                                                                'scripts' => '<script></script>',
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => 'card accordion-pane',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'İnsan Kaynakları',
                                                    'initial_status' => 'closed',
                                                ],
                                            ],
                                            2 => [
                                                'type' => 'accordionpane',
                                                'contents' => [
                                                    0 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => 'detail-name',
                                                                    'name' => 'detail->extras->cookie',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->cookie</print>',
                                                                    'style' => '',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Çerez Metni',
                                                        ],
                                                    ],
                                                    1 => [
                                                        'type' => 'inputwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'input',
                                                                'attributes' => [
                                                                    'class' => 'detail-name',
                                                                    'name' => 'detail->extras->cookie_button',
                                                                    'type' => 'text',
                                                                    'value' => '<print>detail->cookie_button</print>',
                                                                    'style' => '',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Çerez Buton Metni',
                                                        ],
                                                    ],
                                                    2 => [
                                                        'type' => 'selectwithlabel',
                                                        'options' => [
                                                            'html' => [
                                                                'tag' => 'select',
                                                                'attributes' => [
                                                                    'class' => '',
                                                                    'name' => 'detail->extras->cookie_url',
                                                                ],
                                                            ],
                                                            'rules' => '',
                                                            'title' => 'Çerez Bağlantısı',
                                                            'multiple' => '0',
                                                            'value' => '<print>detail->cookie_url</print>',
                                                            'show_default_option' => '0',
                                                            'additional_content' => 'merge',
                                                            'default_value' => '',
                                                            'default_text' => '---Seçiniz---',
                                                        ],
                                                        'data' => [
                                                            'values' => [
                                                                'type' => 'ds:Content->Urls',
                                                            ],
                                                            'stacks' => [
                                                                'scripts' => '<script></script>',
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => 'card accordion-pane',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Çerezler',
                                                    'initial_status' => 'closed',
                                                ],
                                            ],
                                        ],
                                        'options' => [
                                            'html' => [
                                                'tag' => 'div',
                                                'attributes' => [
                                                    'class' => 'accordion row',
                                                    'name' => '',
                                                ],
                                            ],
                                            'rules' => '',
                                        ],
                                    ],
                                    2 => [
                                        'type' => 'clearfix',
                                    ],
                                ],
                            ],
                        ],
                        'params' => [
                            'details' => '<var>sitemap->details</var>',
                        ],
                        'options' => [
                            'html' => [
                                'attributes' => [
                                    'class' => 'tab-list',
                                ],
                            ],
                            'capsulate' => 1,
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Genel',
                    'navigation' => true,
                ],
            ],
            1 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'mpfile',
                        'options' => [
                            'html' => [
                                'tag' => 'input',
                                'attributes' => [
                                    'class' => 'mfile mpimage',
                                    'name' => 'sitemap-><print>sitemap->id</print>',
                                    'type' => 'hidden',
                                ],
                            ],
                            'rules' => '',
                            'tags' => [
                                'cover' => '{"key":"cover","file_type":"image","required":"required","title":"Varsayılan Kapak Görseli","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}',
                                'about_home_banner' => '{"key":"about_home_banner","file_type":"image","required":"","title":"Anasayfa Hakkımızda (1920x1080px)","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}',
                                'news_home_banner' => '{"key":"news_home_banner","file_type":"image","required":"","title":"Haberler Banner (1920x1080px)","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}',
                                'hr_home_banner' => '{"key":"hr_home_banner","file_type":"image","required":"","title":"İnsan Kaynakları Banner (1920x1080px)","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}',
                            ],
                        ],
                        'params' => [
                            'files' => '<var>sitemap->mfiles</var>',
                        ],
                    ],
                    1 => [
                        'type' => 'detailtabs',
                        'contents' => [
                            0 => [
                                'type' => 'tab',
                                'contents' => [
                                    0 => [
                                        'type' => 'mpfile',
                                        'options' => [
                                            'html' => [
                                                'tag' => 'input',
                                                'attributes' => [
                                                    'class' => 'mfile mpimage',
                                                    'name' => 'detail',
                                                    'type' => 'hidden',
                                                ],
                                            ],
                                            'tags' => [
                                                'cover' => '{%quot%key%quot%:%quot%cover%quot%,%quot%file_type%quot%:%quot%image%quot%,%quot%required%quot%:%quot%required%quot%,%quot%title%quot%:%quot%Alternatif Kapak Görseli%quot%,%quot%allow_actions%quot%:[%quot%select%quot%,%quot%upload%quot%],%quot%allow_diskkeys%quot%:[%quot%azure%quot%,%quot%local%quot%],%quot%extensions%quot%:%quot%JPG,PNG,GIF,SVG%quot%,%quot%min_width%quot%:%quot%%quot%,%quot%max_width%quot%:%quot%%quot%,%quot%min_height%quot%:%quot%%quot%,%quot%max_height%quot%:%quot%%quot%,%quot%width%quot%:%quot%%quot%,%quot%height%quot%:%quot%%quot%,%quot%min_filesize%quot%:%quot%%quot%,%quot%max_filesize%quot%:%quot%5120%quot%,%quot%max_file_count%quot%:%quot%1%quot%,%quot%additional_rules%quot%:%quot%%quot%}',
                                            ],
                                        ],
                                        'params' => [
                                            'files' => '<var>detail->mfiles</var>',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'params' => [
                            'details' => '<var>sitemap->details</var>',
                        ],
                        'options' => [
                            'html' => [
                                'attributes' => [
                                    'class' => 'tab-list',
                                ],
                            ],
                            'capsulate' => 0,
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane',
                            'name' => '',
                        ],
                    ],
                    'title' => 'Görseller',
                ],
            ],
            2 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'detailtabs',
                        'contents' => [
                            0 => [
                                'type' => 'tab',
                                'contents' => [
                                    0 => [
                                        'type' => 'detailmetascontrolv2',
                                        'params' => [
                                            'detail' => '<var>detail</var>',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'tab-list',
                                    'name' => '',
                                ],
                            ],
                            'capsulate' => '0',
                            'meta_variables' => '1',
                        ],
                        'params' => [
                            'keyname' => 'key',
                            'itemname' => 'detail',
                            'details' => '<var>sitemap->details</var>',
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'SEO',
                ],
            ],
            3 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'contentprotectioncontrol',
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'col-6',
                                    'name' => '',
                                ],
                            ],
                        ],
                        'params' => [
                            'object' => '<var>sitemap</var>',
                        ],
                    ],
                    1 => [
                        'type' => 'clearfix',
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => '',
                                    'name' => '',
                                ],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane position-relative',
                            'name' => '',
                            'style' => 'min-height:400px;',
                        ],
                    ],
                    'title' => 'Yayınla',
                ],
            ],
        ],
    ],
]

/*EDITOR*/






























            ]
        ];

    }
}
