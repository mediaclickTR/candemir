<?php

namespace App\Modules\Content\Website1\AllBuilder\Renderables\Cleaning;


use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Categories extends BuilderRenderable
{
    public function defaultContents()
    {
        extract($this->params);
        return [
            [
                "type" => "form",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "method" => "post",
                            "action" => route("Content.categories.category.store", ["id" => $category->id]),
                        ]
                    ],
                    "collectable_as"=>["categoriesform", "form"]
                ],
                "contents" =>
                /*EDITOR*/
[
    0 => [
        'type' => 'steptabs',
        'contents' => [
            0 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'contentstatuscontrol',
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'checkbox',
                                    'name' => '',
                                ],
                            ],
                            'rules' => '',
                            'title' => '',
                            'value' => '',
                            'default' => '2',
                            'multiline' => '0',
                        ],
                        'params' => [
                            'values' => '[]',
                            'content_model' => '<var>category</var>',
                        ],
                    ],
                    1 => [
                        'type' => 'div',
                        'contents' => [
                            0 => [
                                'type' => 'detailtabs',
                                'contents' => [
                                    0 => [
                                        'type' => 'tab',
                                        'contents' => [
                                            0 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => 'detail-name',
                                                            'name' => 'detail->name',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->name</print>',
                                                        ],
                                                    ],
                                                    'title' => 'Başlık',
                                                    'rules' => '',
                                                ],
                                            ],
                                            1 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => 'detail->extras->summary',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->summary</print>',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Özet Bilgi',
                                                ],
                                            ],
                                            2 => [
                                                'type' => 'detailslugcontrolv2',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'initial_mode' => 'waiting',
                                                ],
                                                'params' => [
                                                    'detail' => '<var>detail</var>',
                                                ],
                                            ],
                                            3 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => 'detail->extras->sub_title',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->sub_title</print>',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Alt Başlık',
                                                ],
                                            ],
                                            4 => [
                                                'type' => 'ckeditor',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'textarea',
                                                        'attributes' => [
                                                            'class' => 'form-control ckeditor',
                                                            'name' => 'detail->detail',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Detay Yazısı',
                                                    'value' => ' <print>detail->detail</print> ',
                                                ],
                                            ],
                                            5 => [
                                                'type' => 'clearfix',
                                            ],
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'tab-list',
                                            'name' => '',
                                        ],
                                    ],
                                    'capsulate' => '1',
                                    'meta_variables' => '1',
                                ],
                                'params' => [
                                    'keyname' => 'key',
                                    'itemname' => 'detail',
                                    'details' => '<var>category->details</var>',
                                ],
                            ],
                        ],
                        'options' => [
                            'html' => [
                                'attributes' => [
                                    'class' => 'contents',
                                ],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Genel',
                    'navigation' => true,
                ],
            ],
            1 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'mpfile',
                        'options' => [
                            'html' => [
                                'tag' => 'input',
                                'attributes' => [
                                    'class' => 'mfile mpimage',
                                    'name' => 'category-><print>category->id</print>',
                                    'type' => 'hidden',
                                ],
                            ],
                            'rules' => '',
                            'tags' => [
                                'cover' => '{%quot%key%quot%:%quot%cover%quot%,%quot%file_type%quot%:%quot%image%quot%,%quot%required%quot%:%quot%required%quot%,%quot%title%quot%:%quot%Görsel%quot%,%quot%allow_actions%quot%:[%quot%select%quot%,%quot%upload%quot%],%quot%allow_diskkeys%quot%:[%quot%azure%quot%,%quot%local%quot%],%quot%extensions%quot%:%quot%JPG,PNG,GIF,SVG%quot%,%quot%min_width%quot%:%quot%%quot%,%quot%max_width%quot%:%quot%%quot%,%quot%min_height%quot%:%quot%%quot%,%quot%max_height%quot%:%quot%%quot%,%quot%width%quot%:%quot%%quot%,%quot%height%quot%:%quot%%quot%,%quot%min_filesize%quot%:%quot%%quot%,%quot%max_filesize%quot%:%quot%5012%quot%,%quot%max_file_count%quot%:%quot%1%quot%,%quot%additional_rules%quot%:%quot%%quot%}',
                                'banner' => '{%quot%key%quot%:%quot%banner%quot%,%quot%file_type%quot%:%quot%image%quot%,%quot%required%quot%:%quot%%quot%,%quot%title%quot%:%quot%Kategori Banner%quot%,%quot%allow_actions%quot%:[%quot%select%quot%,%quot%upload%quot%],%quot%allow_diskkeys%quot%:[%quot%azure%quot%,%quot%local%quot%],%quot%extensions%quot%:%quot%JPG,PNG,GIF,SVG%quot%,%quot%min_width%quot%:%quot%%quot%,%quot%max_width%quot%:%quot%%quot%,%quot%min_height%quot%:%quot%%quot%,%quot%max_height%quot%:%quot%%quot%,%quot%width%quot%:%quot%%quot%,%quot%height%quot%:%quot%%quot%,%quot%min_filesize%quot%:%quot%%quot%,%quot%max_filesize%quot%:%quot%5120%quot%,%quot%max_file_count%quot%:%quot%1%quot%,%quot%additional_rules%quot%:%quot%%quot%}',
                            ],
                        ],
                        'params' => [
                            'files' => '<var>category->mfiles</var>',
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane',
                            'name' => '',
                        ],
                    ],
                    'title' => 'Görsel',
                ],
            ],
        ],
    ],
]

/*EDITOR*/





























            ]
        ];

    }
}
