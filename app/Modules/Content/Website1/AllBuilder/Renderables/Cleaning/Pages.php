<?php

namespace App\Modules\Content\Website1\AllBuilder\Renderables\Cleaning;


use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Pages extends BuilderRenderable
{
    public function defaultContents()
    {
        extract($this->params);
        return [
            [
                "type" => "form",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "method" => "post",
                            "action" => route("Content.pages.update", ["sitemap_id" => $page->sitemap_id, "id" => $page->id]),
                        ]
                    ],
                    "collectable_as"=>["pagesform", "form"]
                ],
                "contents" =>

                /*EDITOR*/
[
    0 => [
        'type' => 'steptabs',
        'contents' => [
            0 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'toggleswitch',
                        'options' => [
                            'html' => [
                                'tag' => 'input',
                                'attributes' => [
                                    'class' => 'form-check-input',
                                    'name' => 'page-><print>page->id</print>->cvar_1',
                                    'type' => 'checkbox',
                                ],
                            ],
                            'rules' => '',
                            'value' => '<print>page->cvar_1</print>',
                            'title' => 'Detay Linki Var/Yok',
                        ],
                        'data' => [
                            'on_value' => '1',
                            'on_text' => '<u>Detay Linki Yok</u>',
                            'off_value' => '0',
                            'off_text' => '<u>Detay Linki Var</u>',
                        ],
                    ],
                    1 => [
                        'type' => 'div',
                        'contents' => [
                            0 => [
                                'type' => 'detailtabs',
                                'contents' => [
                                    0 => [
                                        'type' => 'tab',
                                        'contents' => [
                                            0 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => 'detail-name',
                                                            'name' => 'detail->name',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->name</print>',
                                                        ],
                                                    ],
                                                    'title' => 'Başlık',
                                                    'rules' => '',
                                                ],
                                            ],
                                            1 => [
                                                'type' => 'detailslugcontrolv2',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'initial_mode' => 'waiting',
                                                ],
                                                'params' => [
                                                    'detail' => '<var>detail</var>',
                                                ],
                                            ],
                                            2 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => 'detail->extras->sub_title',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->sub_title</print>',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Alt Başlık',
                                                ],
                                            ],
                                            3 => [
                                                'type' => 'ckeditor',
                                                'params' => [
                                                ],
                                                'options' => [
                                                    'rules' => '',
                                                    'title' => 'Detay Yazısı',
                                                    'value' => '<print>detail->detail</print>',
                                                    'html' => [
                                                        'attributes' => [
                                                            'name' => 'detail->detail',
                                                        ],
                                                    ],
                                                    'tags' => [
                                                    ],
                                                ],
                                            ],
                                            4 => [
                                                'type' => 'clearfix',
                                            ],
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'tab-list',
                                            'name' => '',
                                        ],
                                    ],
                                    'capsulate' => '1',
                                    'meta_variables' => '1',
                                ],
                                'params' => [
                                    'keyname' => 'key',
                                    'itemname' => 'detail',
                                    'details' => '<var>page->details</var>',
                                ],
                            ],
                        ],
                        'options' => [
                            'html' => [
                                'attributes' => [
                                    'class' => 'contents',
                                ],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Genel',
                    'navigation' => true,
                ],
            ],
            1 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'pagecategoriescontrol',
                        'options' => [
                            'html' => [
                                'tag' => 'select',
                                'attributes' => [
                                    'class' => 'multiple',
                                    'name' => 'page-><print>page->id</print>->sync:categories[]',
                                    'multiple' => 'multiple',
                                ],
                            ],
                            'title' => 'Kategori Seçimi',
                            'multiple' => '1',
                            'show_default_option' => '0',
                            'default_value' => '',
                            'default_text' => '---Seçiniz---',
                            'caption_mode' => 'last_two',
                        ],
                        'params' => [
                            'page_model' => '<var>page</var>',
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane',
                            'name' => '',
                        ],
                    ],
                    'title' => 'Kategoriler',
                ],
            ],
            2 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'mpfile',
                        'options' => [
                            'html' => [
                                'tag' => 'input',
                                'attributes' => [
                                    'class' => 'mfile mpimage',
                                    'name' => 'page-><print>page->id</print>',
                                    'type' => 'hidden',
                                ],
                            ],
                            'rules' => '',
                            'tags' => [
                                'cover' => '{"key":"cover","file_type":"image","required":"required","title":"Varsayılan Kapak Fotoğrafı","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}',
                                'banner' => '{"key":"banner","file_type":"image","title":"Banner Görseli (1920 x 450 px)","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"","max_file_count":"1","additional_rules":""}',
                                'gallery' => '{"key":"gallery","file_type":"image","title":"Galeri","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"","max_file_count":"100","additional_rules":""}',
                            ],
                        ],
                        'params' => [
                            'files' => '<var>page->mfiles</var>',
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Fotoğraflar',
                ],
            ],
            3 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'detailtabs',
                        'contents' => [
                            0 => [
                                'type' => 'tab',
                                'contents' => [
                                    0 => [
                                        'type' => 'detailmetascontrolv2',
                                        'options' => [
                                            'html' => [
                                                'tag' => 'div',
                                                'attributes' => [
                                                    'class' => 'col-12',
                                                    'name' => '',
                                                ],
                                            ],
                                        ],
                                        'params' => [
                                            'detail' => '<var>detail</var>',
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'tab-pane',
                                            'name' => '',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'id' => 5894295543823,
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'tab-list',
                                    'name' => '',
                                ],
                            ],
                            'capsulate' => '0',
                            'meta_variables' => '0',
                        ],
                        'params' => [
                            'keyname' => 'key',
                            'itemname' => 'detail',
                            'details' => '<var>page->details</var>',
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane',
                            'name' => '',
                        ],
                    ],
                    'title' => 'SEO',
                ],
            ],
            4 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'div',
                        'contents' => [
                            0 => [
                                'type' => 'contentprotectioncontrol',
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'col-sm-12 col-md-6',
                                            'name' => '',
                                        ],
                                    ],
                                ],
                                'params' => [
                                    'object' => '<var>page</var>',
                                ],
                            ],
                            1 => [
                                'type' => 'div',
                                'contents' => [
                                    0 => [
                                        'type' => 'pagestatuscontrol',
                                        'params' => [
                                            'page_model' => '<var>page</var>',
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'col-sm-12 col-md-6',
                                            'name' => '',
                                        ],
                                    ],
                                ],
                            ],
                            2 => [
                                'type' => 'div',
                                'contents' => [
                                    0 => [
                                        'type' => 'button',
                                        'options' => [
                                            'html' => [
                                                'tag' => 'button',
                                                'attributes' => [
                                                    'class' => 'btn btn-primary float-right',
                                                    'name' => '',
                                                    'type' => 'submit',
                                                ],
                                            ],
                                            'iconname' => 'check',
                                            'iconsize' => '1',
                                            'button_type' => 'default',
                                            'caption' => 'Kaydet',
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'row-fluid w-100 d-block position-absolute',
                                            'name' => '',
                                            'style' => 'bottom:0;',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'row position-relative',
                                    'name' => '',
                                    'style' => 'overflow:hidden;',
                                ],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Yayınla',
                ],
            ],
        ],
    ],
]

/*EDITOR*/















            ]
        ];

    }
}
