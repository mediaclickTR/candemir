<?php

namespace App\Modules\Content\Website1\AllBuilder\Renderables\Areasweserve;


use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Categories extends BuilderRenderable
{
    public function defaultContents()
    {
        extract($this->params);
        return [
            [
                "type" => "form",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "method" => "post",
                            "action" => route("Content.categories.category.store", ["id" => $category->id]),
                        ]
                    ],
                    "collectable_as"=>["categoriesform", "form"]
                ],
                "contents" =>
                /*EDITOR*/
[
    0 => [
        'type' => 'steptabs',
        'contents' => [
            0 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'contentstatuscontrol',
                        'options' => [
                            'html' => [
                                'tag' => 'div',
                                'attributes' => [
                                    'class' => 'checkbox',
                                    'name' => '',
                                ],
                            ],
                            'rules' => '',
                            'title' => '',
                            'value' => '',
                            'default' => '2',
                            'multiline' => '0',
                        ],
                        'params' => [
                            'values' => '[]',
                            'content_model' => '<var>category</var>',
                        ],
                    ],
                    1 => [
                        'type' => 'div',
                        'contents' => [
                            0 => [
                                'type' => 'detailtabs',
                                'contents' => [
                                    0 => [
                                        'type' => 'tab',
                                        'contents' => [
                                            0 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => 'detail-name',
                                                            'name' => 'detail->name',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->name</print>',
                                                        ],
                                                    ],
                                                    'title' => 'Başlık',
                                                    'rules' => '',
                                                ],
                                            ],
                                            1 => [
                                                'type' => 'detailslugcontrolv2',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'div',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => '',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'initial_mode' => 'waiting',
                                                ],
                                                'params' => [
                                                    'detail' => '<var>detail</var>',
                                                ],
                                            ],
                                            2 => [
                                                'type' => 'inputwithlabel',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'input',
                                                        'attributes' => [
                                                            'class' => '',
                                                            'name' => 'detail->extras->sub_title',
                                                            'type' => 'text',
                                                            'value' => '<print>detail->sub_title</print>',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Alt Başlık',
                                                ],
                                            ],
                                            3 => [
                                                'type' => 'ckeditor',
                                                'options' => [
                                                    'html' => [
                                                        'tag' => 'textarea',
                                                        'attributes' => [
                                                            'class' => 'form-control ckeditor',
                                                            'name' => 'detail->detail',
                                                        ],
                                                    ],
                                                    'rules' => '',
                                                    'title' => 'Detay Yazısı',
                                                    'value' => ' <print>detail->detail</print> ',
                                                ],
                                            ],
                                            4 => [
                                                'type' => 'clearfix',
                                            ],
                                        ],
                                    ],
                                ],
                                'options' => [
                                    'html' => [
                                        'tag' => 'div',
                                        'attributes' => [
                                            'class' => 'tab-list',
                                            'name' => '',
                                        ],
                                    ],
                                    'capsulate' => '1',
                                    'meta_variables' => '1',
                                ],
                                'params' => [
                                    'keyname' => 'key',
                                    'itemname' => 'detail',
                                    'details' => '<var>category->details</var>',
                                ],
                            ],
                        ],
                        'options' => [
                            'html' => [
                                'attributes' => [
                                    'class' => 'contents',
                                ],
                            ],
                        ],
                    ],
                ],
                'options' => [
                    'title' => 'Genel',
                    'navigation' => true,
                ],
            ],
            1 => [
                'type' => 'tab',
                'contents' => [
                    0 => [
                        'type' => 'mpfile',
                        'options' => [
                            'html' => [
                                'tag' => 'input',
                                'attributes' => [
                                    'class' => 'mfile mpimage',
                                    'name' => 'category-><print>category->id</print>',
                                    'type' => 'hidden',
                                ],
                            ],
                            'rules' => '',
                            'tags' => [
                                'banner' => '{"key":"banner","file_type":"image","title":"Banner Görseli (1920 x 450 px)","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"","max_file_count":"1","additional_rules":""}',
                                'cover' => '{"key":"cover","file_type":"image","required":"required","title":"Görsel","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,PNG,GIF,SVG","min_width":"","max_width":"","min_height":"","max_height":"","width":"","height":"","min_filesize":"","max_filesize":"5012","max_file_count":"1","additional_rules":""}',
                            ],
                        ],
                        'params' => [
                            'files' => '<var>category->mfiles</var>',
                        ],
                    ],
                ],
                'options' => [
                    'html' => [
                        'tag' => 'div',
                        'attributes' => [
                            'class' => 'tab-pane',
                            'name' => '',
                        ],
                    ],
                    'title' => 'Görsel',
                ],
            ],
        ],
    ],
]

/*EDITOR*/




























            ]
        ];

    }
}
