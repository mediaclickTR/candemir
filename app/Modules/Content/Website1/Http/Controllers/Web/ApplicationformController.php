<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Heraldist\Models\Form;

class ApplicationformController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress,Request $request){
        if ($request->get("success") == "true")
        {
            return view('web.pages.contact.thanks');
        }
        $extras = [
            "extras"=>
                [
                    [
                        'field_key'=>'extra_fields[]',
                        'field_value'=>[
                            'key'=>'departments',
                            'label'=>'Departman',
                            'attr_type' => 'text',
                            'value'=>$request->get("department")
                        ]
                    ]
                ]
        ];
        $form = Form::where('slug','application-form')->first();
        return view('web.pages.hr.job-form',compact('form','extras'));
	}





}
