<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;

class ServicesController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;
        $pages = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->get();
        $security = Sitemap::where("id",SECURITY_SMID)->with("detail","extras")->first();
        $cleaning = Sitemap::where("id",CLEANING_SMID)->with("detail","extras")->first();

        return view("web.pages.services.index", compact("pages","sitemap", "security","cleaning"));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $page = Page::with('detail.extras', 'extras')
            ->status(1)
            ->find($mediapress->parent->id);

        return view("web.pages.services.detail", compact("page","sitemap", "others"));
    }

}
