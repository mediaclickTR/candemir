<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;

class LogosController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {
        $sitemap = Sitemap::where('id',$mediapress->sitemap->id)->with('detail')->first();
        $logos = Page::where('sitemap_id',$mediapress->sitemap->id)->status(1)->orderBy('order')->with('detail','extras')->get();



        return view("web.pages.logos",compact('sitemap','logos'));
    }

    public function PageDetail(Mediapress $mediapress){


	}



}
