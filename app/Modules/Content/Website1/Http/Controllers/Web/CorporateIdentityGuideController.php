<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;

class CorporateIdentityGuideController extends BaseController
{


    public function SitemapDetail(Mediapress $mediapress)
    {
        $pages = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->get();
        $sitemap = $mediapress->sitemap;

        return view("web.pages.corpate_identity_guides.index", compact("pages","sitemap","others"));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        notFound();
    }
}
