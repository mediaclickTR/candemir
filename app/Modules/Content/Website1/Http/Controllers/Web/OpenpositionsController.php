<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use App\Modules\Content\Website1\AllBuilder\Renderables\News\Categories;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Heraldist\Models\Form;
use Mediapress\Modules\Locale\Models\Province;

class OpenpositionsController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress,Request $request)
    {

        $sitemap = Sitemap::where('id',$mediapress->sitemap->id)->with('detail')->first();
        Province::find(2);
        $provinces = Province::where('country_id',223)->pluck('name')->toArray();
        array_unshift($provinces,"null");
        $categories = Category::where('sitemap_id',$mediapress->sitemap->id)
            ->with(['detail','pages' => function ($query){
            $query->with('detail')->status(1)->get();
        }])->get();

        if ($request->get("success") == "true")
        {
            return view('web.pages.contact.thanks');
        }
        $extras = [
            "extras"=>
                [
                    [
                        'field_key'=>'extra_fields[]',
                        'field_value'=>[
                            'key'=>'departments',
                            'label'=>'Departman',
                            'attr_type' => 'text',
                            'value'=>$request->get("department")
                        ]
                    ]
                ]
        ];
        $form = Form::where('slug','application-form')->first();

        return view("web.pages.hr.open-positions",compact('sitemap','categories','provinces','form','extras'));
    }




}
