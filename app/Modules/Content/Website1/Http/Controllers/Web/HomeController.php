<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Scene;

class HomeController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {

        $sliders = Scene::where('slider_id', 1)
            ->where('status', 1)
            ->whereHas('detail')
            ->with('detail')
            ->orderBy("order","ASC")
            ->get();

        $home = $mediapress->sitemap;

        $top_news = Page::where("sitemap_id",NEWS_SMID)->status("1")->with("detail","extras")->status(1)->orderBy("id","DESC")->take(4)->get();
        $bottom_news = Page::where("sitemap_id",NEWS_SMID)->status(1)->where("cvar_1",1)->with("detail","extras")->take(3)->get();
        return view("web.pages.home", compact("sliders", "home","top_news","bottom_news"));
	}
}
