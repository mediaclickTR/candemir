<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;

class PolicyController extends BaseController
{
    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = Sitemap::where('id',$mediapress->sitemap->id)->with('detail')->first();

        $page = Page::with('detail.extras', 'extras')
            ->status(1)
            ->find($mediapress->parent->id);

        return view("web.pages.policy.detail", compact("sitemap","page"));
    }
}
