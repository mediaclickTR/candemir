<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;

class SecurityController extends BaseController
{
    public function SitemapDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;

        $others = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->orderBy("order","ASC")->with("detail","extras")->get();

        $areas_we_serve_categories = Sitemap::where("id",AREAS_WE_SERVE)->first()->categories()->status(1)->get();

        return view("web.pages.services.security_home", compact("sitemap","others","areas_we_serve_categories"));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;

        $page = Page::with('detail.extras', 'extras')
            ->find($mediapress->parent->id);

        $others = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->get();

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>getUrlBySitemapId(SERVICE_SMID), "name"=>langPart("breadcrumb.services", "Hizmetlerimiz")],
            ["url"=>getUrlBySitemapId(SECURITY_SMID), "name"=>getSitemapName(SECURITY_SMID)]
        ];

        if ($page){
            $breadcrumb[]= ["url"=>$page->detail->url, "name"=>$page->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;

        return view("web.pages.services.security", compact("page","sitemap","others"));
    }
}
