<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Heraldist\Models\Form;

class ContactController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress,Request $request){

            if ($request->get("success") == "true")
            {
                return view('web.pages.contact.thanks');
            }
            $form = Form::find(1);
            $sitemap = Sitemap::where('id',$mediapress->sitemap->id)->with('detail')->first();
            return view('web.pages.contact.contact',compact('sitemap','form'));




	}





}
