<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;

class ManagementController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress) {

        $pages = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->orderBy("order","ASC")->with("detail","extras")->get();

        $sitemap = $mediapress->sitemap;

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>getUrlBySitemapId(ABOUT_SMID_2), "name"=>langPart("breadcrumb.about", "Hakkımızda")]
        ];

        if ($sitemap){
            $breadcrumb[]= ["url"=>$sitemap->detail->url, "name"=>$sitemap->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;

        return view("web.pages.about.management", compact("pages","sitemap"));
	}

}
