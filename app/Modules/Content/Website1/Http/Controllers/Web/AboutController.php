<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\Sitemap;

class AboutController extends BaseController
{
    public function SitemapDetail(Mediapress $mediapress)
    {
        $page = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->first();
        if ($page){
            return redirect()->to($page->detail->url);
        }
        return redirect()->to(getUrlBySitemapId(ABOUT_SMID_2));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = Sitemap::where('id',$mediapress->sitemap->id)->with('detail')->first();

         $page = Page::with('detail.extras', 'extras')
            ->status(1)
            ->find($mediapress->parent->id);

        return view("web.pages.about.detail", compact("sitemap","page"));
	}
}
