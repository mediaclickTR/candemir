<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;

class CleaningController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {
        $pages = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->get();

        $sitemap = $mediapress->sitemap;
        $categories = $sitemap->categories()->status(1)->get();

        $others = Page::where("sitemap_id",SECURITY_SMID)->status("1")->with("detail","extras")->get();

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>getUrlBySitemapId(SERVICE_SMID), "name"=>langPart("breadcrumb.services", "Hizmetlerimiz")],
        ];

        if ($sitemap){
            $breadcrumb[]= ["url"=>$sitemap->detail->url, "name"=>$sitemap->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;

        return view("web.pages.services.cleaning..index", compact("pages","sitemap","categories","others"));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;

        $page = Page::with('detail.extras', 'extras')
            ->find($mediapress->parent->id);

        $category = $page->categories()->first();

        $others = Page::where("sitemap_id",SECURITY_SMID)->status("1")->with("detail","extras")->get();

        $other_pages = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->get();

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>getUrlBySitemapId(SERVICE_SMID), "name"=>langPart("breadcrumb.services", "Hizmetlerimiz")],
        ];

        if ($sitemap){
            $breadcrumb[]= ["url"=>$sitemap->detail->url, "name"=>$sitemap->detail->name];
        }

        if ($category){
            $breadcrumb[]= ["url"=>$category->detail->url, "name"=>$category->detail->name];
        }

        if ($page){
            $breadcrumb[]= ["url"=>$page->detail->url, "name"=>$page->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;

        return view("web.pages.services.cleaning.detail", compact("page","sitemap","other_pages","others"));
    }

    public function CategoryDetail(Mediapress $mediapress)
    {
        $category = Category::where("id",$mediapress->parent->id)->with("detail","extras")->first();

        $sitemap = $mediapress->sitemap;
        $pages = $category->pages()->status(1)->get();
        $others = Page::where("sitemap_id",SECURITY_SMID)->status("1")->with("detail","extras")->get();
        $other_categories =  Category::where("sitemap_id",$mediapress->sitemap->id)->with("detail","extras")->get();

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>getUrlBySitemapId(SERVICE_SMID), "name"=>langPart("breadcrumb.services", "Hizmetlerimiz")],
        ];

        if ($sitemap){
            $breadcrumb[]= ["url"=>$sitemap->detail->url, "name"=>$sitemap->detail->name];
        }

        if ($category){
            $breadcrumb[]= ["url"=>$category->detail->url, "name"=>$category->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;

        return view("web.pages.services.cleaning.category", compact("pages","sitemap","category","others","other_categories"));
    }

}
