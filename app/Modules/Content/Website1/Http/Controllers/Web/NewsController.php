<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;

class NewsController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {
        $model = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras");

        $years = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->select(\DB::raw('YEAR(date) as year'))->distinct()->get();

        $pages = $model->paginate(20);

        $sitemap = $mediapress->sitemap;

        $breadcrumb = [
            ["url"=>$mediapress->homePageUrl()->url, "name"=>langPart("breadcrumb.home", "Anasayfa")],
            ["url"=>"javascript:void()", "name"=>langPart("breadcrumb.press.room", "Basın Odası")],
        ];

        if ($sitemap){
            $breadcrumb[]= ["url"=>$sitemap->detail->url, "name"=>$sitemap->detail->name];
        }

        $mediapress->data['breadcrumb'] = $breadcrumb;


        return view("web.pages.news.index", compact("pages","sitemap","years"));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;

        $page = Page::with('detail.extras', 'extras')
            ->find($mediapress->parent->id);

        $others = Page::where("sitemap_id",$mediapress->sitemap->id)->where("id","!=",$page->id)->status("1")->with("detail","extras")->get();

        return view("web.pages.news.detail", compact("page","sitemap","others"));
    }

    public function CategoryDetail(Mediapress $mediapress){


	}

    public function getNews(Request $request)
    {
        $pages = Page::with(['details', 'extras'])->where("sitemap_id", NEWS_SMID);

        if (isset($request->month) && $request->city != "0") {
            $pages->whereMonth('date', '=', $request->month);
        }

        if (isset($request->year) && $request->year != "0") {
            $pages->whereYear('date', '=', $request->year);
        }

        $pages = $pages->status(1)->distinct()->orderBy("order", "ASC")->paginate(20);

        return view("web.pages.news.ajax_list", compact("pages"));
    }

}
