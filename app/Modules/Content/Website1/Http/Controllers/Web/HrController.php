<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;

class HrController extends BaseController
{
    public function SitemapDetail(Mediapress $mediapress)
    {
        $page = Page::where("sitemap_id",$mediapress->sitemap->id)->status("1")->with("detail","extras")->first();
        if ($page){
            return redirect()->to($page->detail->url);
        }
        return redirect()->to(getUrlBySitemapId(SERVICE_SMID));
    }

    public function PageDetail(Mediapress $mediapress)
    {
        $sitemap = $mediapress->sitemap;
        $page = Page::with('detail.extras', 'extras')
            ->find($mediapress->parent->id);

        return view("web.pages.hr.detail", compact("page","sitemap"));
	}
}
