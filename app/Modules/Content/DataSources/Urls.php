<?php

namespace App\Modules\Content\DataSources;

use Illuminate\Support\Facades\Cache;
use Mediapress\Foundation\DataSource;
use Mediapress\Modules\MPCore\Models\Url;

class Urls extends DataSource
{
    public function getData()
    {
        return Cache::rememberForever(hash('md5', 'MediapressUrls' ), function () {
            $data = [];
            $urls = Url::get();
            foreach ($urls as $url) {
                $data[$url['id']] = $url->url;
            }
            return $data;
        });
    }
}
