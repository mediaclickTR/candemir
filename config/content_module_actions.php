<?php
return [

    "websites" => [
        'name' => 'ContentPanel::auth.sections.websites',
        'name_affix' => "MPCorePanel::auth.general.shared_abilities",
        'item_model' => \Mediapress\Modules\Content\Models\Website::class,
        'item_id' => "*",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'create' => [
                'name' => "MPCorePanel::auth.actions.create",
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update",
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ]
        ],
        'subs' => [
            "sitemaps" => [
                'name' => 'ContentPanel::auth.sections.sitemaps',
                'name_affix' => "MPCorePanel::auth.general.shared_abilities",
                'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => true,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                    'activate_variation' => [
                        'name' => 'Varyasyonu Etkinleştir'
                    ],
                    'deactivate_variation' => [
                        'name' => 'Varyasyonu Kull. Dışı Bırak'
                    ]
                ],
                'subs' => [
                    'details' => [
                        'name' => 'ContentPanel::auth.sections.sitemap_details',
                        'item_model' => \Mediapress\Modules\Content\Models\SitemapDetail::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            /*'edit_apply' =>  [
                            'name' => "Güncelleme Öner",
                            'variables' =>['languages']
                            ],*/
                            'edit_update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                                'variables' => ['languages']
                            ]
                        ],
                        'settings' => [
                            "auth_view_collapse" => "in"
                        ]

                    ],
                    "pages" => [
                        'name' => 'ContentPanel::auth.sections.pages',
                        'item_model' => \Mediapress\Modules\Content\Models\Page::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ],
                        ],
                        'subs' => [
                            'detail' => [
                                'name' => 'ContentPanel::auth.sections.page_detail',
                                'item_model' => \Mediapress\Modules\Content\Models\PageDetail::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                                    ],*/
                                    'edit_update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                        'variables' => ['languages']
                                    ]
                                ],
                                'settings' => [
                                    "auth_view_collapse" => "in"
                                ],
                            ],

                        ],
                        'settings' => [
                            "auth_view_collapse" => "in"
                        ],
                    ],
                    "categories" => [
                        'name' => 'ContentPanel::auth.sections.categories',
                        'item_model' => \Mediapress\Modules\Content\Models\Category::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'edit' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'reorder' => [
                                'name' => "MPCorePanel::auth.actions.reorder",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                            'detail' => [
                                'name' => 'ContentPanel::auth.sections.category_detail',
                                'item_model' => \Mediapress\Modules\Content\Models\CategoryDetail::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                                    ],*/
                                    'edit_update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                        'variables' => ['languages']
                                    ]
                                ],
                            ],

                        ],
                    ],
                    "criterias" => [
                        'name' => 'ContentPanel::auth.sections.criterias',
                        'item_model' => \Mediapress\Modules\Content\Models\Criteria::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'edit' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'reorder' => [
                                'name' => "MPCorePanel::auth.actions.reorder",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                            'detail' => [
                                'name' => 'ContentPanel::auth.sections.criteria_detail',
                                'item_model' => \Mediapress\Modules\Content\Models\CriteriaDetail::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    /*'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                                    ],*/
                                    'edit_update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                        'variables' => ['languages']
                                    ]
                                ],
                            ],

                        ],
                    ],
                    "properties" => [
                        'name' => 'ContentPanel::auth.sections.properties',
                        'item_model' => \Mediapress\Modules\Content\Models\Property::class,
                        'data' => [
                            "is_root" => false,
                            "is_variation" => false,
                            "is_sub" => true,
                            "descendant_of_sub" => true,
                            "descendant_of_variation" => false
                        ],
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                            ],
                            'edit' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                            'reorder' => [
                                'name' => "MPCorePanel::auth.actions.reorder",
                            ],
                            'delete' => [
                                'name' => "MPCorePanel::auth.actions.delete",
                            ]
                        ],
                        'subs' => [
                            'detail' => [
                                'name' => 'ContentPanel::auth.sections.property_detail',
                                'item_model' => \Mediapress\Modules\Content\Models\PropertyDetail::class,
                                'data' => [
                                    "is_root" => false,
                                    "is_variation" => false,
                                    "is_sub" => true,
                                    "descendant_of_sub" => true,
                                    "descendant_of_variation" => false
                                ],
                                'actions' => [
                                    /* 'edit_apply' =>  [
                                    'name' => "Güncelleme Öner",
                                    'variables' =>['languages']
                                    ],*/
                                    'edit_update' => [
                                        'name' => "MPCorePanel::auth.actions.update",
                                        'variables' => ['languages']
                                    ]
                                ],
                            ],

                        ],
                    ],

                ],
                'variations' => [],
                'settings' => [
                    "auth_view_collapse" => "in"
                ]

            ],
            "country_groups" => [
                'name' => 'ContentPanel::auth.sections.country_groups',
                'name_affix' => "",
                'item_model' => \Mediapress\Modules\MPCore\Models\CountryGroup::class,
                'item_id' => "*",
                'data' => [
                    "is_root" => false,
                    "is_variation" => false,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ],
                "actions" => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ],
                ]
            ],
            "menus" => [
                'name' => 'ContentPanel::auth.sections.menus',
                'item_model' => \Mediapress\Modules\Content\Models\Menu::class,
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                        'variables' => []
                    ],
                    'create' => [
                        'name' => "MPCorePanel::auth.actions.create",
                        'variables' => []
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update"
                    ],
                    'delete' => [
                        'name' => "MPCorePanel::auth.actions.delete",
                    ]
                ],
                'subs' => [
                ],
            ],

            "seo" => [
                'name' => 'ContentPanel::auth.sections.seo',
                'item_model' => \Mediapress\Modules\MPCore\Models\Meta::class,
                'actions' => [
                    'index' => [
                        'name' => "MPCorePanel::auth.actions.list",
                        'variables' => []
                    ],
                    'update' => [
                        'name' => "MPCorePanel::auth.actions.update",
                    ],
                    'export' => [
                        'name' => "Excel'e Aktar",
                    ],
                ],
                'subs' => [
                    'sitemap_xmls' => [
                        'name' => 'ContentPanel::auth.sections.sitemap_xmls',
                        'item_model' => \Mediapress\Modules\Content\Models\SitemapXml::class,
                        'actions' => [
                            'index' => [
                                'name' => "MPCorePanel::auth.actions.list",
                                'variables' => []
                            ],
                            'create' => [
                                'name' => "MPCorePanel::auth.actions.create",
                                'variables' => []
                            ],
                            'update' => [
                                'name' => "MPCorePanel::auth.actions.update",
                            ],
                        ],
                        'subs' => [],
                        'variations' => []
                    ]
                ]
            ],
        ],
        'variations_title'=>'ContentPanel::auth.sections.recorded_websites',
        'variations' => function ($root_item) {
            $websites = \Mediapress\Modules\Content\Models\Website::with('detail')->get();
            $sets = [];

            foreach ($websites as $website) {
                /*if (!$website->detail) {
                    continue;
                }*/
                $set = [];
                $set["name"] = 'ContentPanel::auth.sections.website';
                $set["name_affix"] = $website->name;
                $set['item_model'] = \Mediapress\Modules\Content\Models\Website::class;
                $set['item_id'] = $website->id;
                $set['actions'] = [
                    'update' => ['name' => "MPCorePanel::auth.actions.update",],
                    'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                ];
                $set['settings'] = [
                    "auth_view_collapse" => "out"
                ];
                $set['data'] = [
                    "is_root" => false,
                    "is_variation" => true,
                    "is_sub" => false,
                    "descendant_of_sub" => false,
                    "descendant_of_variation" => false
                ];
                $set['subs'] = $root_item['subs'];
                $set['subs']['sitemaps']['variations'] = function ($root_item) {

                    /*if($root_item["item_id"]=="*"){
                        echo $root_item["name"];
                        return [];
                    }*/

                    $sitemaps = \Mediapress\Modules\Content\Models\Sitemap::with('detail')->get();
                    $sets = [];

                    foreach ($sitemaps as $sm) {
                        if (!$sm->detail) {
                            continue;
                        }
                        $set = [];
                        $set["name"] = 'ContentPanel::auth.sections.sitemap';
                        $set["name_affix"] = $sm->detail->name;
                        $set['item_model'] = \Mediapress\Modules\Content\Models\Sitemap::class;
                        $set['item_id'] = $sm->id;
                        $set['actions'] = [
                            'update' => ['name' => "MPCorePanel::auth.actions.update",],
                            'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                        ];
                        $set['settings'] = [
                            "auth_view_collapse" => "out"
                        ];
                        $set['data'] = [
                            "is_root" => false,
                            "is_variation" => true,
                            "is_sub" => false,
                            "descendant_of_sub" => false,
                            "descendant_of_variation" => false
                        ];
                        $set['subs'] = $root_item['subs'];
                        data_set($set['subs'], '*.data.descendant_of_variation', true);
                        $sets['sitemap' . $sm->id] = $set;
                    }

                    return $sets;
                };
                $set['subs']['sitemaps']['variations_title'] = 'ContentPanel::auth.sections.recorded_sitemaps';
                $set['subs']['seo']['subs']['sitemap_xmls']['variations'] = function ($root_item) {
                    $websites = \Mediapress\Modules\Content\Models\Website::with(['detail', 'sitemapxmls'])->get();
                    $sets = [];

                    foreach ($websites as $website) {
                        $sitemap_xmls = \Mediapress\Modules\Content\Models\SitemapXml::where("website_id", $website->id)->get();
                        foreach ($sitemap_xmls as $sitemap_xml) {
                            $set = [];
                            $set["name"] = 'ContentPanel::auth.sections.sitemap_xml';
                            $set["name_affix"] = "(". $sitemap_xml->title .") ".$website->name;
                            $set['item_model'] = \Mediapress\Modules\Content\Models\Website::class;
                            $set['item_id'] = $website->id;
                            $set['actions'] = [
                                'update' => ['name' => "MPCorePanel::auth.actions.update",],
                                'delete' => ['name' => "MPCorePanel::auth.actions.delete",]
                            ];
                            $set['settings'] = [
                                "auth_view_collapse" => "out"
                            ];
                            $set['data'] = [
                                "is_root" => false,
                                "is_variation" => true,
                                "is_sub" => false,
                                "descendant_of_sub" => false,
                                "descendant_of_variation" => false
                            ];
                            $set['subs'] = $root_item['subs'];

                            data_set($set['subs'], '*.data.descendant_of_variation', true);

                            $sets['sitemap_xml' . $sitemap_xml->id] = $set;
                        }
                    }

                    return $sets;
                };
                $set['subs']['seo']['subs']['sitemap_xmls']['variations_title'] = 'ContentPanel::auth.sections.recorded_sitemap_xmls';
                data_set($set['subs'], '*.data.descendant_of_variation', true);

                $sets['website' . $website->id] = $set;
            }

            return $sets;
        },
    ],
    /*"settings" => [
    'name' => 'Ayarlar',
    'item_model' => 'Mediapress\Models\Setting',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    ],
    'subs' => [
    'langs' => [
    'name' => 'Diller',
    'item_model' => \Mediapress\Modules\MPCore\Models\Language::class,
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    ]

    ],
    'lang_parts' => [
    'name' => 'Dil Değişkenleri',
    'item_model' => \Mediapress\Modules\MPCore\Models\LanguagePart::class,
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ]
    ]
    ],
    'urls' => [
    'name' => 'URL İşlemleri',
    'item_model' => \Mediapress\Modules\MPCore\Models\Url::class,
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'view_url_list' => [
    'name' => "Url Listesi"
    ],
    ]
    ],
    'upfiles' => [
    'name' => 'Yüklenen Dosyalar',
    'item_model' => null,
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'view' => [
    'name' => 'Görüntüle'
    ],
    'delete_not_on_disk' => [
    'name' => 'Diskte Bulunmayanları Sil'
    ],
    'delete_not_on_db' => [
    'name' => 'Veritabanında Bulunmayanları Sil'
    ],
    'delete_inconsistent' => [
    'name' => 'Tüm Bulunamayanları Sil'
    ]
    ],
    ]
    ]
    ],*/
    /*"script_manager" => [
    'name' => 'Script Yöneticisi',
    'item_model' => 'Mediapress\Models\Script',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ],
    ],
    "slider" => [
    'name' => 'Slider',
    'item_model' => 'Mediapress\Models\Slider',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ],
    'subs' => [
    'part' => [
    'name' => 'Slide',
    'item_model' => 'Mediapress\Models\SliderPart',
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'subs' => [
    'detail' => [
    'name' => 'Slide Detay',
    'item_model' => 'Mediapress\Models\SliderPartDetail',
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => true,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ],
    ]
    ]
    ]
    ]
    ],
    "popup" => [
    'name' => 'Pop-up',
    'item_model' => 'Mediapress\Models\Popup',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ]
    ],
    "sidebar" => [
    'name' => 'Sidebar',
    'item_model' => 'Mediapress\Models\Sidebar',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ]
    ],*/
    /*
    "warehouse" => [
    'name' =>'Depo'
    ],
    */
    /*"page_subscription" => [
    'name' => 'Sayfa Aboneliği',
    'item_model' => 'Mediapress\Models\PageSubscription',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ],
    'export' => [
    'name' => "Excele Aktar",
    'variables' => ['file_type']
    ]
    ],
    ],*/
    /*"photogallery" => [
    'name' => 'Foto Galeri',
    'item_model' => 'Mediapress\Models\PageGallery',
    'subs' => [
    'detail' => [
    'name' => 'Galeri Detay',
    'item_model' => 'Mediapress\Models\PageGalleryDetail',
    'data' => [
    "is_root" => false,
    "is_variation" => false,
    "is_sub" => true,
    "descendant_of_sub" => false,
    "descendant_of_variation" => false
    ],
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ],
    'edit_update' => [
    'name' => "MPCorePanel::auth.actions.update",
    'variables' => ['languages']
    ]
    ],
    'subs' => [
    'detail' => [
    'name' => 'Galeri Detay Görseller',
    'item_model' => 'Mediapress\Models\PageGalleryImage',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    ]
    ]
    ]
    ],
    ]
    ],*/
    /*"user_process" => [
    'name' => 'Üye İşlemleri',
    'item_model' => \Mediapress\Modules\Content\Models\Sitemap::class,

    'subs' => [
    "users" => [
    'name' => 'Üyeler',
    'item_model' => \Mediapress\Modules\Entity\Models\User::class,
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ]
    ],
    "dues" => [
    'name' => 'Aidatlar',
    'item_model' => 'Mediapress\Models\Due',
    'actions' => [
    'index' => [
    'name' => "MPCorePanel::auth.actions.list",
    'variables' => []
    ],
    'create' => [
    'name' => "MPCorePanel::auth.actions.create",
    'variables' => []
    ],
    'update' => [
    'name' => "MPCorePanel::auth.actions.update"
    ],
    'debit' => [
    'name' => "Borçlandır"
    ],
    'delete' => [
    'name' => "MPCorePanel::auth.actions.delete",
    ]
    ]
    ],

    ],
    ],*/
    /*
    "members" => [
    'name' =>'Üyeler'
    ],
    "tech" => [
    'name' =>'Teknik Yönetim'
    ],
    "dues" => [
    'name' =>'Aidatlar'
    ],
    */
];
