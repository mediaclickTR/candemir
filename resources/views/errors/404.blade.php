
@php
if(!isset($mediapress)){
$mediapress = mediapress();
}
@endphp
@extends('web.inc.app')

@push("styles")
    <link rel="stylesheet" href="{!! asset("assets/css/style-404.css") !!}">
@endpush

@section('content')
    <div class="page" id="page-404">
        <div class="top-head">
            <div class="img">
                <img src="/assets/img/top-header.jpg" alt="">
            </div>
        </div>
        <div class="sitearea">
            <div class="title">
                4<span>0</span>4
            </div>
            <div class="desc">
                <p>Lütfen ulaşmak istediğiniz sayfanın adresini doğru yazdığınızı kontrol edin.</p>
                <p>Eğer doğru adresi yazdığınıza eminseniz, ulaşmak istediğiniz sayfa silinmiş olabilir.</p>
            </div>
            <div class="mc-link white">
                <a href="{!! $mediapress->homePageUrl !!}">
                    <span>Anasayfa'ya Dön</span>
                </a>
            </div>
        </div>
    </div>
@endsection