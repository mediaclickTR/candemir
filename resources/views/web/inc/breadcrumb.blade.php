@php
    if (isset($mediapress->data["breadcrumb"])){
        $breadcrumb = $mediapress->data["breadcrumb"];
    }else{
        $breadcrumb = $mediapress->breadcrumb();
    }
@endphp
<div class="bread">
    <ul>
        @foreach($breadcrumb as $data)
            <li><a href="{!! $data['url'] !!} ">{!! $data['name'] !!}</a></li>
        @endforeach
    </ul>
</div>
