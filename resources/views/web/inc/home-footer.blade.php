@php
    $footer_menus = \Cache::remember(md5('iyilik'. 'footer'. $mediapress->activeLanguage->id . $mediapress->url->website_id), 10, function() use($mediapress) {
        return $mediapress->menu('footer');
    });

     $contact = Cache::remember(md5('candemir'. 'contact'. $mediapress->activeLanguage->id . $mediapress->url->website_id), 10, function() use($mediapress) {
            return getSitemapById(CONTACT_SMID);
     });
@endphp

<div class="step" id="step5">
    <div class="foot">
        <div class="row">
            <div class="col-lg-5">
                <div class="address follow-canvas">
                    {!! $contact->detail->detail !!}
                    <p>
                    {!! langPart("footer.phone","Telefon:") !!} <a href="tel:{!! strip_tags($contact->detail->phone) !!}">{!! $contact->detail->phone !!}</a>
                    <p>
                        {!! langPart("footer.email","E-Posta:") !!} <a href="mailto:{!! strip_tags($contact->detail->email) !!}">{!! $contact->detail->email !!}</a>
                    </p>
                    <div class="mc-link">
                        <a href="{!! $contact->ctex_1 !!}" target="_blank">
                            <span>{!! langPart("view.map","Haritayı Görüntüle") !!}</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 offset-2">
                <div class="title">{!! langPart("footer.ebulletin.title","E-Bültene Kayıt Ol") !!}</div>
                <form action="#" method="get">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" id="ebulletin-email" placeholder="{!! strip_tags(langPart("footer.ebulletin.email.placeholder","E-Posta")) !!}">
                        <div id="alert_msg"></div>
                    </div>
                    <div class="form-group">
                        <button type="button" id="bulten-gonder">{!! langPart("footer.ebulletin.send","Gönder") !!}</button>
                    </div>
                </form>
                <div class="social">
                    <ul class="social">
                        @if(count(socialMedia())>0)
                            @foreach(socialMedia() as $social)
                                <li>
                                    <a href="{!! $social['link'] !!}" target="_blank"><i class="fab fa-{!! $social['name'] !!}{!! ($social['name']=="facebook") ? '-f' : '' !!}{!! ($social['name']=="linkedin") ? '-in' : '' !!}"></i></a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copy">
        <ul>
            <li>{!! langPart("footer.copyright","Candemir Holding, Her hakkı saklıdır.") !!}</li>
            @foreach($footer_menus as $menu)
                @if($menu->status != 1) @continue @endif
                @php
                    if(is_null($menu->type)){ $url='javascript:void(0);'; }
                    else{ $url = ($menu->url) ? $menu->url->url : 'javascript:void(0);'; }
                @endphp
                <li>
                    <a href="{!! $url !!}">{!! $menu->name !!}</a>
                </li>
            @endforeach
            <li><a href="https://www.mediaclick.com.tr" target="_blank">{!! langPart("footer.web.design","Web Tasarım") !!}</a> <a href="https://www.mediaclick.com.tr" target="_blank">MediaClick</a></li>
        </ul>
    </div>
    <div id="map"></div>
</div>

@push("scripts")
    <script>
        $(document).ready(function() {
            $('#bulten-gonder').click(function (e) {
                e.preventDefault();
                if (($('#ebulletin-email').val().length != 0)) {
                    var form = $('#ebulten');
                    var email = $('#ebulletin-email').val();
                    var token = $("input[name=_token]").val();
                    $.ajax({
                        method: "POST",
                        url: '{{ route('ebulletin.sender') }}',
                        data: {"email": email, "_token": token},
                        cache: false
                    }).fail(function (msg) {
                        $('#alert_msg').css({
                            'background-color': '#721c24',
                            'color': '#FFF',
                            'padding': '10px 20px',
                        });
                        var message = '';
                        if (msg.status == 422) {
                            var data = JSON.parse(msg.responseText);
                            for (var i in data) {
                                message += '<li><span class="fa fa-hand-o-right"></span> ' + data[i] + '</li>';
                                $('#alert_msg').fadeIn("slow");
                                $('#alert_msg').fadeIn(5000);
                                if (data[i][0] == "The E-Mail must be a valid email address.") {
                                    $('#alert_msg').html('{!! langPartAttr('ebulletin.valid.email.address') !!}');
                                } else {
                                    $('#alert_msg').html('{!! langPartAttr('ebulletin.email.already.been.registered') !!}');
                                }
                            }
                        }
                    }).done(function (data) {
                        // Redirect page
                        window.location.href = "{!! getUrlBySitemapId(EBULLETIN_SMID) !!}";
                    });
                }else{
                    $('#alert_msg').css({
                        'background-color': '#721c24',
                        'color': '#FFF',
                        'padding': '10px 20px',
                    });
                    $('#alert_msg').html('{!! langPartAttr('ebulletin.required.email.address',"Lütfen bir e-posta giriniz.") !!}');
                }
            });
        });
    </script>
@endpush

