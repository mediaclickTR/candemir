@php
$areas_we_serve = getSitemapById(AREAS_WE_SERVE);
$cleaning = getSitemapById(CLEANING_SMID);
@endphp
@if(isset($others))
<div class="tabmenu">
    <ul>
        {{--<li @if($mediapress->relation->slug == $cleaning->detail->slug) class="active" @endif ><a href="{!! $cleaning->detail->url !!}">{!! $cleaning->detail->name !!}</a></li>--}}
        @foreach($others as $other)
            <li @if($mediapress->relation->slug == $other->detail->slug) class="active" @endif ><a href="{!! $other->detail->url !!}">{!! $other->detail->name !!}</a></li>
        @endforeach
        <li @if($mediapress->sitemap->id == $areas_we_serve->sitemap->id) class="active" @endif ><a href="{!! $areas_we_serve->detail->url !!}">{!! $areas_we_serve->detail->name !!}</a></li>
    </ul>
</div>
@endif
