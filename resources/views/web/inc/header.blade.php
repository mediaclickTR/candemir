@section('header')

    @php
        $header_menus = \Cache::remember(md5('candemir'. 'header'. $mediapress->activeLanguage->id . $mediapress->url->website_id), 10, function() use($mediapress) {
            return $mediapress->menu('header');
        });
    @endphp

    <header class="header">
        <div class="top-bar">
            <div class="logo">
                <a href="{!! getUrlBySitemapId(HOME_SMID) !!}">
                    <img src="{!! asset("assets/img/logo.svg") !!}" alt="{!! strip_tags(langPart("header.logo.title","Candemir Holding")) !!}">
                </a>
            </div>
            <div class="menus">
                <ul>
                    @foreach($header_menus as $menu)
                    @if($menu->status != 1) @continue @endif
                    @php
                        if(is_null($menu->type)){ $url='javascript:void(0);'; }
                        else{ $url = ($menu->url) ? $menu->url->url : 'javascript:void(0);'; }

                        if ($menu->children->isNotEmpty() && getUserAgent() == "mobile"){
                            $url = 'javascript:void(0);';
                        }
                    @endphp
                    <li>
                        <a href="{!! $url !!}">{!! $menu->name !!}</a>
                        @if($menu->children->isNotEmpty() && $menu->type_view == 0)
                            <ul>
                                @foreach($menu->children as $child)
                                    @if($child->status != 1) @continue @endif
                                    @php

                                        if(is_null($child->type)){ $child_url='javascript:void(0);'; }
                                        elseif($child->type == 1){ $child_url=$child->out_link; }
                                        elseif(!isset($child->url->url)){$child_url = "javascript:void(0);";}
                                        else{$child_url = $child->url->url;}
                                    @endphp
                                    <li><a href="{!! $child_url !!}">{!! $child->name !!}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="left-bar">
            <ul class="social disabled">
                @if(count(socialMedia())>0)
                        @foreach(socialMedia() as $social)
                            <li>
                                <a href="{!! $social['link'] !!}" target="_blank"><i class="fab fa-{!! $social['name'] !!}{!! ($social['name']=="facebook") ? '-f' : '' !!}"></i></a>
                            </li>
                        @endforeach
                @endif
            </ul>
            <div class="title-canvas disabled"></div>
        </div>
        <div class="right-bar">
            <div class="menu-bars">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <div class="lang" style="display: none;">
                @foreach($mediapress->otherLanguages(0) as $otherLang)
                    @if($mediapress->activeLanguage->code == $otherLang['language_code']) @continue @endif
                    <a href="{!! url($otherLang['url']->url) !!}"><img src="{!! asset("assets/img/lang/".$otherLang['language_code'].".svg") !!}" alt="{!! strtoupper($otherLang['language_code']) !!}"></a>
                @endforeach
                <a href="{!! getUrlBySitemapId(HOME_SMID) !!}">
                </a>
            </div>
            <div class="steps-nav disabled">
                <i class="top"></i>
                <i class="bottom active"></i>
            </div>
        </div>
    </header>

@endsection
