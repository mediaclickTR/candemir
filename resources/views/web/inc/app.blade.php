@extends('mediapress::main')
@php
    $home = Cache::remember(md5('candemir'. 'home'. $mediapress->activeLanguage->id . $mediapress->url->website_id), 10, function() use($mediapress) {
           return getSitemapById(HOME_SMID);
        });
@endphp
@prepend('styles')

    <link rel="shortcut icon" href="{!! asset("assets/img/favicon.png") !!}">
    <link rel="apple-touch-icon" href="{!! asset("assets/img/favicon.png") !!}">
    <meta name="theme-color" content="#161616">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap|animate|owl-carousel|slick|fancybox|style-global.css?v=10')}}" />
@endprepend

@include("web.inc.header")

@include("web.inc.footer")

@prepend('scripts')

    <link rel="stylesheet" href="{{asset('assets/css/responsive.css?v=4')}}" />

    <script>
        /*window.cookieconsent_options = {
            message: '{!! $home->detail->cookie !!} <a class="seeour" target="_blank" href="{!! detailUrlRedirect($home->detail->cookie_url) !!}">{!! langPart("see.our.cookie.policty","Cookie politikası") !!}</a>',
            dismiss: '{!! $home->detail->cookie_button !!}',
            theme: 'dark-bottom'
        };*/
    </script>

    <script src="{!! asset("assets/js/jquery|bootstrap|owl-carousel|slick|fancybox.js") !!}"></script>
    {{--<script src="{!! asset("assets/js/cookieconsent|scripts-global.js") !!}"></script>--}}
    <script src="{!! asset("assets/js/scripts-global.js?v=2") !!}"></script>

@endprepend
