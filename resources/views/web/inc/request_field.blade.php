<div class="quote">
    <div class="t">
        {!! langPart("security.bottom","Şirketiniz İçin Güvenlik Hizmetlerine mi İhtiyacınız Var?") !!}
    </div>
    <div class="d">
        {!! langPart("security.bottom.detail","Şirketinizin güvenlik hizmetleri için Ulusal Özel Güvenlik’ten ücretsiz fiyat teklifi alın. Ulusal Özel Güvenlik Satış Temsilcisi sizinle en kısa sürede iletişime geçecektir.") !!}
    </div>
    <a href="{!! getUrlBySitemapId(CONTACT_SMID) !!}">{!! langPart("scurity.free","Ücretsiz Fiyat Teklifi") !!}</a>
</div>
