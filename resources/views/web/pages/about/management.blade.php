@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style-governance.css') }}"/>
@endpush
@section('content')
    <div class="page" id="page-governance">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include('web.inc.breadcrumb')
            </div>
        </div>
        <div class="sitearea">
            {!! $sitemap->detail->detail !!}
            <div class="row">
                @foreach($pages as $page)
                <div class="col-lg-4">
                    <div class="b">
                        <div class="img">
                            <img src="{!! image($page->f_cover) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                        </div>
                        <div class="texts">
                            <h2>{!! $page->detail->name !!}</h2>
                            <span>{!! $page->detail->degree !!}</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection


