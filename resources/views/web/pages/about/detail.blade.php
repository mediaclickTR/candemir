@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style-governance.css') }}"/>
@endpush
@section('content')
    <div class="page">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($page->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $page->detail->name !!}</h1>
                @include('web.inc.breadcrumb')
            </div>
        </div>
        <div class="sitearea">
            {!! $page->detail->detail !!}
        </div>
    </div>
@endsection


