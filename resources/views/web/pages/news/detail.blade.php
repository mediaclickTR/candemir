@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style-news-detail.css') }}"/>
@endpush
@section('content')
    <div class="page" id="page-news-detail">
        <div class="top-head">
            <div class="img">
                <img src="{!! ($page->f_banner) ? image($page->f_banner)->resize(["w"=>1867]) : image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $page->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <div class="details">
                @if($page->date)
                    @php($date = explode("-",$page->date))
                    <div class="news-date">{!! $date[2].".".$date[1].".".$date[0] !!}</div>
                @endif
                <h2>{!! $page->detail->name !!}</h2>
                <p>{!! $page->detail->summary !!}</p>
                @if($page->f_cover)
                    <img src="{!! image($page->f_cover) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                @endif
                {!! $page->detail->detail !!}
                <div class="mc-link white">
                    <a href="{!! getUrlBySitemapId(NEWS_SMID) !!}">
                        <span>{!! langPart("return.back","Geri Dön") !!}</span>
                    </a>
                </div>
            </div>
            <div class="sidebar">
                <div class="title">{!! langPart("other.news","Diğer Haberler") !!}</div>
                <ul>
                    @foreach($others as $other)
                        <li><a href="{!! $other->detail->url !!}">{!! $other->detail->name !!}</a></li>
                    @endforeach
                    <li><a href="{!! getUrlBySitemapId(NEWS_SMID) !!}"><i
                                class="fas fa-arrow-right"></i> {!! langPart("all.news","Tüm Haberler") !!}</a></li>
                </ul>
            </div>
            <div class="shares">
                <b>{!! strip_tags(langPart("share","Paylaş")) !!}</b>
                <ul>
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={!! url($page->detail->url) !!}" target="_blank"><i class="fab fa-facebook-f"></i></a> </li>
                    <li><a href="https://twitter.com/intent/tweet?url={!! url($page->detail->url) !!}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://web.whatsapp.com/send?text={!! url($page->detail->url) !!}" data-action="share/whatsapp/share" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                    <li><a href="http://pinterest.com/pin/create/link/?url={!! $page->detail->url !!}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection
