@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style-news.css') }}"/>
@endpush
@section('content')
    <div class="page" id="page-news">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <div class="toparea">
                <h2>{!! strip_tags(langPart("recently.news","Güncel Haberler")) !!}</h2>
                <div class="filtre">
                    <div class="f">
                        <select name="month" id="month" onchange="ajaxLoad()">
                            <option value="0" selected disabled>{!! strip_tags(langPart("month","Ay")) !!}</option>
                            @php($iter = 1)
                            @foreach(getMonths() as $month)
                            <option value="{!! $iter !!}">{!! translateDate("F", $month) !!}</option>
                            @php($iter++)
                            @endforeach
                        </select>
                    </div>
                    <div class="f">
                        <select name="year" id="year" onchange="ajaxLoad()">
                            <option selected disabled>{!! strip_tags(langPart("year","Yıl")) !!}</option>
                            @foreach($years as $year)
                            <option value="{!! $year->year !!}">{!! $year->year !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
                <div class="row" id="content">
                    @include("web.pages.news.ajax_list")
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        function ajaxPaginate(paginate)
        {
            ajaxLoad(paginate);
        }

        function ajaxLoad($page = 1)
        {
            var month = $("#month").val();
            var year = $("#year").val();
            var page = $page;

            var data_ = {"month": month, "year":year,"page": page};

            $.ajax({
                type: "GET",
                url: "{!! route("getNews") !!}",
                data: data_,
                success: function (data) {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $(".page").offset().top
                    }, 1000);
                    $("#content").html(data);
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    </script>

    <script src="{{asset('assets/js/ahoselector.js')}}"></script>
@endpush
