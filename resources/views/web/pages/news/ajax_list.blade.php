@if(count($pages)>0)
    @foreach($pages as $page)
        <div class="col-lg-4">
            <div class="box">
                <a href="{!! $page->detail->url !!}">
                    <div class="img">
                        <img src="{!! image($page->f_cover)->resize(["w"=>500]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                        <div class="news-date">
                            @if($page->date)
                                @php($date = explode("-",$page->date))
                                <b>{!! $date[2] !!}</b>
                                <span>{!! translateDate("M", $page->date) !!}</span>
                            @endif
                        </div>
                    </div>
                    <h3>{!! $page->detail->name !!}</h3>
                    <p>{!! $page->detail->summary !!}</p>
                </a>
            </div>
        </div>
    @endforeach
    {!! $pages->links() !!}
@else
    <div class="alert alert-warning">
        {!! langPart("news.not.found","İlgili tarihlere ait haber bulunamadı.") !!}
    </div>
@endif
