@extends('web.inc.app')
@push("styles")
    <link rel="stylesheet" href="{!! asset("assets/css/style-home.css?v=6") !!}">
@endpush
@section('content')
    <div class="page" id="page-home">
        <div class="steps">
            <div class="step" id="step1" >
                <div class="owl-carousel bg-slider">
                    @foreach($sliders as $slider)
                        @php
                            $detail = $slider->detail;
                            $slider = new \Mediapress\Modules\Content\Foundation\Slide([
                            'time'=> $slider->time,
                            'texts'=> $detail->texts,
                            'buttons'=> $detail->buttons,
                            'files'=> $detail->files,
                            'url'=> $detail->url
                            ]);
                        @endphp
                        <div class="item">
                            <div class="img">
                                <img src="/assets/img/lazyload.svg" data-src="{!! $slider->image() !!}" alt="{!!  strip_tags($slider->texts[1][getUserAgent()])  !!}">
                                <div class="texts">
                                    <h2 class="follow-canvas">{{  $slider->texts[1][getUserAgent()]  }}</h2>
                                    <p>{!! $slider->texts[2][getUserAgent()] !!} </p>
                                    @if($slider->url["button"][1]["text"] != "" && $slider->url["button"][1]["text"] != null)
                                    <div class="mc-link">
                                        <a href="{!! $slider->url["button"][1]["text"] !!}">
                                            <span>{!! $slider->buttons[1][getUserAgent()] !!}  </span>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="bottom-slider-nav">
                    <ul>
                        @php $counter=1 @endphp
                        @foreach($sliders as $slider)
                            @php
                                $detail = $slider->detail;
                                $slider = new \Mediapress\Modules\Content\Foundation\Slide([
                                'time'=> $slider->time,
                                'texts'=> $detail->texts,
                                'buttons'=> $detail->buttons,
                                'files'=> $detail->files,
                                'url'=> $detail->url
                                ]);
                            @endphp

                            <li data-slider-page="{!! $counter++ !!}" data-slider-url="{!! $slider->url["button"][1]["text"] !!}" @if($loop->first) class="active" @endif>
                                <div class="centered">
                                    <div class="ico"></div>
                                    <div class="title">
                                        @if(isset($slider->texts[3]))
                                            {!! $slider->texts[3][getUserAgent()] !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="bar"></div>
                            </li>

                        @endforeach
                    </ul>
                    <div class="news">
                        <div class="owl-carousel">
                            @foreach($top_news as $new)
                                <div class="item">
                                    <a href="{!! $new->detail->url !!}">{!! $new->detail->name !!}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="step" id="step2" style="background-image: url('{!! image($home->f_about_home_banner)->resize(["w"=>1872]) !!}');">
                <div class="contentarea">
                    <div class="texts">
                        <div class="title">
                            <span class="follow-canvas">{!! $home->detail->about_title  !!}</span>
                            <b>{!! $home->detail->about_title_2 !!}</b>
                        </div>
                        <p>{!! $home->detail->about_detail !!}</p>
                        <div class="mc-link">
                            <a href="{!! detailUrlRedirect($home->detail->button_url) !!}"><span>{!! $home->detail->button_title !!}</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="step" id="step3" style="background-image: url('{!! image($home->f_hr_home_banner)->resize(["w"=>1872]) !!}');">
                <div class="contentarea">
                    <div class="texts">
                        <div class="title">
                            <span class="follow-canvas">{!! $home->detail->hr_title_1 !!}</span>
                            <b>{!! $home->detail->hr_subtitle !!}</b>
                        </div>
                        <p>{!! $home->detail->hr_detail !!}</p>
                    </div>
                    <div class="ik">
                        <div class="items">
                            <div class="item">
                                <a href="{!! detailUrlRedirect($home->detail->hr_url_1) !!}">
                                    <div class="cls">
                                        <i></i>
                                    </div>
                                    <div class="fixwidth">
                                        <div class="ico"></div>
                                    </div>
                                    <div class="t">{!! $home->detail->hr_title_1 !!}</div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{!! detailUrlRedirect($home->detail->hr_url_2) !!}">
                                    <div class="cls">
                                        <i></i>
                                    </div>
                                    <div class="fixwidth">
                                        <div class="ico"></div>
                                    </div>
                                    <div class="t">{!! $home->detail->hr_title_2 !!}</div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{!! detailUrlRedirect($home->detail->hr_url_3) !!}">
                                    <div class="cls">
                                        <i></i>
                                    </div>
                                    <div class="fixwidth">
                                        <div class="ico"></div>
                                    </div>
                                    <div class="t">{!! $home->detail->hr_title_3 !!}</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="step" id="step4" style="background-image: url('{!! image($home->f_news_home_banner)->resize(["w"=>1872]) !!}');">
                <div class="contentarea">
                    <div class="texts">
                        <div class="title">
                            <h1 class="follow-canvas">{!! langPart("home.news","Haberler") !!}</h1>
                        </div>
                        <p>{!! getSitemapById(NEWS_SMID)->detail->detail !!}</p>
                        <div class="mc-link">
                            <a href="{!! getUrlBySitemapId(NEWS_SMID) !!}"><span>{!! langPart("home.news.see.more","Daha Fazla Gör") !!}</span></a>
                        </div>
                        <div class="row">
                            @foreach($bottom_news as $news)
                                <div class="col-lg-4">
                                    <div class="box">
                                        <a href="{!! $news->detail->url !!}">
                                            <div class="img">
                                                <img src="/assets/img/lazyload.svg" data-src="{!! image($news->f_cover)->resize(["w"=>407]) !!}" alt="{!! strip_tags($news->detail->name) !!}">
                                                <div class="news-date">
                                                    @if($news->date)
                                                        @php($date = explode("-",$news->date))
                                                        <b>{!! $date[2] !!}</b>
                                                        <span>{!! translateDate("M", $news->date) !!}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <h2>{!! $news->detail->name !!}</h2>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @include("web.inc.home-footer")
        </div>
    </div>

    @push("scripts")
        @if(!isMobile())
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCa9WrG968zb0WtQJucpz2MW18oK3Ctkfc&sensor=false"></script>
        @endif
        <script src="{!! asset("assets/js/scripts-home.js") !!}"></script>
    @endpush

@endsection
