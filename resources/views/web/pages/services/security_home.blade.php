@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services-security.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services-security">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <div class="row">
                <div class="col-lg-9">
                    <h2>{!! $sitemap->detail->sub_title !!}</h2>
                    {!! $sitemap->detail->detail !!}

                    @if($sitemap->f_gallery)
                        <div class="owl-carousel images">
                            @if(isImageObject($sitemap->f_gallery))
                                @foreach($sitemap->f_gallery as $image)
                                    <div class="item">
                                        <div class="img">
                                            <img src="{!! image($image) !!}" alt="{!! strip_tags($sitemap->detail->name)  !!}">
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="item">
                                    <div class="img">
                                        <img src="{!! image($sitemap->f_gallery) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
                                    </div>
                                </div>
                            @endif
                        </div>

                    @endif
                    @if($sitemap->f_cover)
                        <img src="{!! image($sitemap->f_cover) !!}" alt="">
                    @endif

                    @foreach($others as $page)
                        <h2>{!! $page->detail->name !!}</h2>
                        {!! $page->detail->detail !!}

                        @if($page->f_gallery)
                            <div class="owl-carousel images">
                                @if(isImageObject($page->f_gallery))
                                    @foreach($page->f_gallery as $image)
                                        <div class="item">
                                            <div class="img">
                                                <img src="{!! image($image) !!}" alt="{!! strip_tags($page->detail->name)  !!}">
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="item">
                                        <div class="img">
                                            <img src="{!! image($page->f_gallery) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif

                        @if($page->f_slide)
                            <div class="owl-carousel slide">
                                @if(isImageObject($page->f_slide))
                                    @foreach($page->f_slide as $image)
                                        <div class="item">
                                            <div class="img">
                                                <img src="{!! image($image) !!}" alt="{!! strip_tags($page->detail->name)  !!}">
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="item">
                                        <div class="img">
                                            <img src="{!! image($page->f_slide) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif

                        @if($page->f_cover)
                            <img src="{!! image($page->f_cover) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                        @endif
                    @endforeach

                    <div class="mc-link white margin">
                        <a href="{!! getUrlBySitemapId(SERVICE_SMID) !!}"><span>{!! langPart("return.back","Geri Dön") !!}</span></a>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="sidebar">
                        <ul>
                            @foreach($areas_we_serve_categories as $other_cat)
                                <li ><a href="{!! $other_cat->detail->url !!}">{!! $other_cat->detail->name !!}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
        </div>
        </div>

        @include("web.inc.request_field")
    </div>
@endsection
