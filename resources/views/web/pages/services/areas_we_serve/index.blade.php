@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services-security.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services-security">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <h2>{!! $sitemap->detail->sub_title !!}</h2>
            {!! $sitemap->detail->detail !!}

            <div class="boxer">
                <div class="row">
                    @foreach($categories as $category)
                    <div class="col-lg-4">
                        <div class="box">
                            <a href="{!! $category->detail->url !!}">
                                <div class="img">
                                    <img src="{!! image($category->f_cover) !!}" alt="{!! strip_tags($category->detail->name) !!}">
                                </div>
                                <h3>{!! $category->detail->name !!}</h3>
                                <p>{!! shortText($category->detail->detail,100) !!}</p>
                                <span>{!! langPart("show.detail","Detayları Gör ") !!}<i></i></span>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="mc-link white margin">
                <a href="{!! getUrlBySitemapId(SECURITY_SMID) !!}"><span>{!! langPart("return.back","Geri Dön") !!}</span></a>
            </div>
        </div>
        @include("web.inc.request_field")
    </div>
@endsection
