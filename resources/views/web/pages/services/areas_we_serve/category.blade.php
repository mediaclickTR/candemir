@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services-security.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services-security">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($category->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($category->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $category->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>

        <div class="sitearea">

            <div class="row">
            <div class="col-lg-9">

            <h2>{!! $category->detail->sub_title !!}</h2>
            {!! $category->detail->detail !!}

            <div class="boxer">
                <div class="row">
                    @foreach($pages as $page)
                        <div class="col-lg-6">
                            <div class="box">
                                @if($page->cvar_1 == 0)
                                    <a href="{!! $page->detail->url !!}">
                                    @endif
                                    <div class="img">
                                        <img src="{!! image($page->f_cover) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                                    </div>
                                    <h3>{!! $page->detail->name !!}</h3>
                                    @if($page->cvar_1 == 0)
                                    <a href="{!! $page->detail->url !!}">
                                        <span>{!! langPart("show.detail","Detayları Gör ") !!}<i></i></span>
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="mc-link white margin">
                <a href="{!! getUrlBySitemapId(SECURITY_SMID) !!}"><span>{!! langPart("return.back","Geri Dön") !!}</span></a>
            </div>

            </div>

            <div class="col-lg-3">
                <div class="sidebar">
                    <ul>
                        @foreach($other_categories as $other_cat)
                        <li @if($category->id == $other_cat->id) class="active" @endif><a href="{!! $other_cat->detail->url !!}">{!! $other_cat->detail->name !!}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @include("web.inc.request_field")
    </div>
@endsection
