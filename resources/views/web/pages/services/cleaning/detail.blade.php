@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services-security.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services-security">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($page->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $page->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">

            <div class="row">
                <div class="col-lg-9">

                    <h2>{!! $page->detail->sub_title !!}</h2>
                    {!! $page->detail->detail !!}

                    @if($page->f_gallery)
                        <div class="owl-carousel images">
                            @if(isImageObject($page->f_gallery))
                                @foreach($page->f_gallery as $image)
                                    <div class="item">
                                        <div class="img">
                                            <img src="{!! image($image) !!}" alt="{!! strip_tags($page->detail->name)  !!}">
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="item">
                                    <div class="img">
                                        <img src="{!! image($page->f_gallery) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endif

                    <div class="mc-link white margin">
                        <a href="{!! $page->categories()->first()->detail->url !!}"><span>{!! langPart("return.back","Geri Dön") !!}</span></a>
                    </div>

                </div>

                <div class="col-lg-3">
                    <div class="sidebar">
                        <ul>
                            @foreach($other_pages as $other_page)
                                @if($other_page->cvar_1 && $other_page->cvar_1 == 1) @continue @endif
                                <li @if($page->id == $other_page->id) class="active" @endif><a href="{!! $other_page->detail->url !!}">{!! $other_page->detail->name !!}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include("web.inc.request_field")
    </div>
@endsection
