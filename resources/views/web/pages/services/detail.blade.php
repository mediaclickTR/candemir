@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services-detail.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services-detail">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($page->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $page->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            {!! $page->detail->detail !!}

            @if($page->f_gallery)
                <div class="owl-carousel images">
                    @if(isImageObject($page->f_gallery))
                        @foreach($page->f_gallery as $image)
                            <div class="item">
                                <div class="img">
                                    <img src="{!! image($image) !!}" alt="{!! strip_tags($page->detail->name)  !!}">
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="item">
                            <div class="img">
                                <img src="{!! image($page->f_gallery) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                            </div>
                        </div>
                    @endif
                </div>
            @endif

            @if($page->f_cover)
            <img src="{!! image($page->f_cover) !!}" alt="{!! strip_tags($page->detail->name)  !!}">
            @endif
            <div class="mc-link white margin">
                <a href="{!! getUrlBySitemapId(SERVICE_SMID) !!}"><span>{!! langPart("return.back","Geri Dön") !!}</span></a>
            </div>
        </div>
        @include("web.inc.request_field")
    </div>
@endsection
