@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-services.css')}}"/>
@endpush
@section('content')
    <div class="page" id="page-services">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <div class="box">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="img">
                            <img src="{!! image($security->f_list_img)->resize(["w"=>780]) !!}" alt="{!! strip_tags($security->detail->name) !!}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="texts">
                            <h2>{!! $security->detail->name !!}</h2>
                            {!! $security->detail->detail !!}
                            <a href="{!! $security->detail->url !!}">{!! langPart("show.details","Detayları Gör ") !!}<i></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="row">
                    <div class="col-lg-6 order-lg-last">
                        <div class="img">
                            <img src="{!! image($cleaning->f_list_img)->resize(["w"=>780]) !!}" alt="{!!strip_tags($cleaning->detail->name)!!}">
                        </div>
                    </div>
                    <div class="col-lg-6 order-lg-first">
                        <div class="texts">
                            <h2>{!! $cleaning->detail->name !!}</h2>
                            <p>{!! $cleaning->detail->summary !!}</p>
                            <a href="{!! $cleaning->detail->url !!}">{!! langPart("show.details","Detayları Gör ") !!}<i></i></a>
                        </div>
                    </div>
                </div>
            </div>

            @foreach($pages as $page)
            <div class="box">
                <div class="row">
                    <div class="col-lg-6 @if($loop->iteration % 2 == 0) order-lg-last @endif">
                        <div class="img">
                            <img src="{!! image($page->f_list_img)->resize(["w"=>780]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
                        </div>
                    </div>
                    <div class="col-lg-6  @if($loop->iteration % 2 == 0) order-lg-first @endif">
                        <div class="texts">
                            <h2>{!! $page->detail->name !!}</h2>
                            <p>{!! $page->detail->summary !!}</p>
                            <a href="{!! $page->detail->url !!}">{!! langPart("show.details","Detayları Gör ") !!}<i></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @include("web.inc.request_field")
    </div>
@endsection
