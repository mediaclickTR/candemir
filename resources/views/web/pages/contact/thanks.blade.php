@extends('web.inc.app')
@section('content')
<div class="page" id="page-thanks">
    <div class="top-head">
        <div class="img">
            <img src="{{asset('assets/img/top-header.jpg')}}" alt="Teşekkürler">
        </div>
    </div>
    <div class="sitearea">
        <div class="title">
            {{strip_tags(langPart("thanks","Teşekkürler !"))}}
        </div>
        <p>{{strip_tags(langPart("thanks.detail","Bilgileriniz başarılı bir şekilde alınmıştır, en kısa sürede sizlere dönüş sağlanacaktır."))}}</p>
        <div class="mc-link white">
            <a href="{{getUrlBySitemapId(HOME_SMID)}}">
                <span>{{langPart("backtohome","Anasayfa'ya Dön")}}</span>
            </a>
        </div>
    </div>
</div>
@endsection
