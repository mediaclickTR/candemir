@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-contact.css')}}"/>
@endpush
@section('content')
<div class="page" id="page-contact">
    <div id="map"></div>
    <div class="top-head">
        <div class="texts">
            <h1 class="follow-canvas">{!! $sitemap->detail->name !!}</h1>
            @include('web.inc.breadcrumb')
        </div>
        <div class="area">
            <div class="row">
                <div class="col-lg-5">
                    <div class="address">
                        {!! $sitemap->detail->detail !!}
                        <p>
                            {!! langPart("contact.phone","Telefon:") !!} <a href="tel:{!! strip_tags($sitemap->detail->phone) !!}">{!! $sitemap->detail->phone !!}</a>
                        </p>
                        <p>
                            {!! langPart("contact.phone","Telefon:") !!} <a href="tel:+905439074997">+90 543 907 49 97</a>
                        </p>
                        <p>
                            {!! langPart("contact.email","E-Posta:") !!} <a href="mailto:{!! strip_tags($sitemap->detail->email) !!}">{!! $sitemap->detail->email !!}</a>
                        </p>

                        <div class="mc-link">

                            <a href="{!! $sitemap->ctex_1 !!}" target="_blank">
                                <span>{!! strip_tags(langPart("see.map","Haritayı Görüntüle")) !!}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-2">
                    <div class="bcktop">
                        {!! $form->view() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCa9WrG968zb0WtQJucpz2MW18oK3Ctkfc&sensor=false"></script>
    <script src="{{asset('assets/js/ahovalidator.js')}}"></script>
    <script src="{{asset('assets/js/scripts-contact.js')}}"></script>
@endpush


