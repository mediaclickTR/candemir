@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style-logos.css') }}"/>
@endpush
@section('content')
    <div class="page" id="page-logos">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($sitemap->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $sitemap->detail->name !!}</h1>
                @include("web.inc.breadcrumb")
            </div>
        </div>
        <div class="sitearea">
            <div class="row">
                @foreach($pages as $page)
                <div class="col-lg-4">
                    <div class="box file">
                        <div class="img">
                            <div class="text">
                                {!! $page->detail->name !!}
                            </div>
                            <div class="bottom-bar">
                                <a href="{!! $page->f_file !!}" data-fancybox="logos">
                                    <i></i>
                                </a>
                                <a href="{!! $page->f_file !!}" target="_blank" download>
                                    <i></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
