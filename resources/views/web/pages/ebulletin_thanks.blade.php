@extends('web.inc.app')
@push("styles")
    <link rel="stylesheet" href="{{asset('assets/css/style-thanks.css.css')}}">
@endpush
@section('content')
    <div class="page" id="page-thanks">
        <div class="top-head">
            <div class="img">
                <img src="{!! asset("assets/img/top-header.jpg") !!}" alt="{!! strip_tags(langPart("ebulletin.thanks.title","Teşekkürler !")) !!}">
            </div>
        </div>
        <div class="sitearea">
            <div class="title">
                {!! strip_tags(langPartAttr("ebulletin.thanks.title","Teşekkürler !")) !!}
            </div>
            <p>
                {!! strip_tags(langPartAttr("ebulletin.thanks.description","Bilgileriniz başarılı bir şekilde alındı, en kısa sürede sizlere dönüş sağlanacaktır.")) !!}
            </p>
            <div class="mc-link white">
                <a href="{!! getUrlBySitemapId(HOME_SMID) !!}"> <span>{!! langPart('thanks.button','Anasayfa\'ya Dön') !!} </span></a>
            </div>
        </div>
    </div>
@endsection


