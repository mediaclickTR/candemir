@extends('web.inc.app')
@push('styles')
    <link rel="stylesheet" href="{!! asset('assets/css/style-logos.css') !!}"/>
@endpush
@section('content')
<div class="page" id="page-logos">
    <div class="top-head">
        <div class="img">
            <img src="{!! image($sitemap->f_cover) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
        </div>
        <div class="texts">
            <h1>{!! $sitemap->detail->name !!}</h1>
            @include('web.inc.breadcrumb')
        </div>
    </div>
    <div class="sitearea">
        <div class="row">
            @foreach($logos as $logo)
            <div class="col-lg-4">
                <div class="box">
                    <div class="img">
                        <img src="{!! image($logo->f_cover) !!}" alt="{!! strip_tags($logo->detail->name) !!}">
                        <div class="bottom-bar">
                            <a href="{!! image($logo->f_cover) !!}" data-fancybox="logos">
                                <i></i>
                            </a>
                            <a href="{!! image($logo->f_cover)  !!}" target="_blank" download ">
                                <i></i>
                            </a>
                        </div>
                    </div>
                    <h2>{!! $logo->detail->name !!}</h2>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
