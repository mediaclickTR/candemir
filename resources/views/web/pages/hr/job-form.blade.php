@extends('web.inc.app')

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/css/style-openpositions.css')}}">
@endpush

@section('content')
    <div class="page" id="page-openpositions">
        <div class="top-head">
            <div class="img">
                <img src="{{asset('assets/img/top-header.jpg')}}" alt="">
            </div>
            <div class="texts">
                <h1>Güvenlik Görevlisi Başvurusu</h1>
                @include('web.inc.breadcrumb')
            </div>
        </div>
        <div class="sitearea">
            <div class="title">{{ strip_tags(langPart("form.info","Lütfen Aşağıdaki Formu Doldurunuz"))}}</div>
            {!! $form->view($extras) !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('assets/js/ahoselector.js')}}"></script>
@endpush
