@extends('web.inc.app')
    @push('styles')
        <link rel="stylesheet" href="{{asset('assets/css/style-openpositions.css')}}">
    @endpush
@section('content')
<div class="page" id="page-openpositions">
    <div class="top-head">
        <div class="img">
            <img src="{!! image($sitemap->f_cover) !!}" alt="{!! strip_tags($sitemap->detail->name) !!}">
        </div>
        <div class="texts">
            <h1>{!! $sitemap->detail->name !!}</h1>
            @include('web.inc.breadcrumb')
        </div>
    </div>
    <div class="sitearea">
        {{--@foreach($categories as $category)
            @if($category->pages->count() > 0)
        <div class="title">{!! $category->detail->name !!}</div>
        <div class="boxes">
            @foreach($category->pages as $jobs)
            <div class="box">
                <ul>
                    <li><b>{!! $jobs->detail->name !!}</b></li>
                    <li><span>{!! $jobs->detail->department !!}</span></li>
                    <li><span>{!! $provinces[$jobs->cint_2] !!} </span></li>
                    <li><a href="#{!! $jobs->id !!}" data-fancybox="jobs">{!! strip_tags(langPart("information","Bilgi")) !!}</a><a href="{{strip_tags(url(getUrlBySitemapId(APPLICATION_FORM)."?department=".$jobs->detail->department)) }}">{!! strip_tags(langPart("join","Başvur")) !!}</a></li>
                </ul>
                <div class="modal" id="{!! $jobs->id !!}" style="display: none;">
                    <div class="clsr"></div>
                    <h1>{!! $jobs->detail->name !!}</h1>
                    <p>
                        {!! $jobs->detail->detail !!}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
            @endif
        @endforeach--}}
        <div class="title">{{ strip_tags(langPart("form.infoTitle","Açık Pozisyonlar"))}}</div>
        <iframe src="https://www.eleman.net/firma/candemir-holding-f1483554" width="100%" height="260px" frameborder="0" scrolling="auto"></iframe>

        <div class="title mt-5">{{ strip_tags(langPart("form.infoTitle2","Genel Başvuru Formu"))}}</div>
        {!! $form->view($extras) !!}
    </div>
</div>
</div>
@endsection
@push('scripts')
    <script src="{{asset('assets/js/ahovalidator.js')}}"></script>
    <script>
        $(document).ready(function(){
            ahovalidator.build('#page-openpositions form');
        });
    </script>
    <script src="{{asset('assets/js/ahoselector.js')}}"></script>
@endpush


