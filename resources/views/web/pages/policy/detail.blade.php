@extends('web.inc.app')
@section('content')
    <div class="page">
        <div class="top-head">
            <div class="img">
                <img src="{!! image($page->f_banner)->resize(["w"=>1867]) !!}" alt="{!! strip_tags($page->detail->name) !!}">
            </div>
            <div class="texts">
                <h1>{!! $page->detail->name !!}</h1>
                @include('web.inc.breadcrumb')
            </div>
        </div>
        <div class="sitearea">
            @if($page->f_file)
                <div class="mc-link white float-right">
                    <a href="{!! image($page->f_file)->url !!}" download="{{ strip_tags($page->detail->name) }}"><span><i class="fas fa-download mr-1"></i> {!! langPart('policy.filebtn', "Dosya İndir") !!}</span></a>
                </div>
            @endif
            {!! $page->detail->detail !!}
        </div>

    </div>
@endsection
