
if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    $(window).resize(function(){
        $('body,html').stop().animate({ scrollTop: $(window).height()*(stepsPage-1)}, 750);
        setTimeout(function(){
            title_canvas();
        },750);
    });

    function title_canvas(){
        var item = $('#step'+stepsPage+' .follow-canvas');
        $('.header .left-bar .title-canvas').css({
            'top': item.offset().top - $(window).scrollTop(),
            'height': item.height()
        });
        $('#page-home .news').css('width', $('#page-home .steps #step1 .bottom-slider-nav ul li').width()*2 + 3 );
        news.trigger('refresh.owl.carousel');
    }

    $(document).ready(function(){
        $('body,html').animate({scrollTop: 0});
        title_canvas();
        $('body').css('overflow','hidden');
        $('.disabled').removeClass('disabled');
        $('.footer').addClass('disabled');
        step1Slider.trigger('refresh.owl.carousel');
    });

    $('.header .right-bar .steps-nav i.top').click(function(){
        stepsTop($(this));
    });
    $('.header .right-bar .steps-nav i.bottom').click(function(){
        stepsBottom($(this));
    });

    $(document).keydown(function(event){					
        var code = (event.which) ? event.which: event.keyCode;
        if(code==38){
            stepsTop('.header .right-bar .steps-nav i.top');
        }
        if(code==40){
            stepsBottom('.header .right-bar .steps-nav i.bottom');
        }
    });

    function stepsTop(t){
        if (stepsPage > 1) {
            stepsPage--;
        }
        $('body,html').stop().animate({ scrollTop: $(window).height()*stepsPage - $(window).height()}, 750);
        $(t).parent().find('i').addClass('active');
        setTimeout(function(){
            title_canvas();
        },750);
    }

    function stepsBottom(t){
        $('body,html').stop().animate({ scrollTop: $(window).height()*stepsPage }, 750);
        $(t).parent().find('i').addClass('active');
        if (stepsPage < 5) {
            stepsPage++;
        }
        setTimeout(function(){
            title_canvas();
        },750);
    }

    setInterval(function(){
        if (stepsPage == 1) {
            $('.header .right-bar .steps-nav i.top').removeClass('active');
        }
        if (stepsPage == 5) {
            $('.header .right-bar .steps-nav i.bottom').removeClass('active');
        }
    },10);

    var scrollTimeout;

    $(".steps").bind('wheel', function(event) {
        clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(function(){
            if (event.originalEvent.deltaY <= 0) {
                if (stepsPage != 1) {
                    stepsTop('.header .right-bar .steps-nav i.top');
                }
            }else{
                if (stepsPage != 5) {
                    stepsBottom('.header .right-bar .steps-nav i.bottom');
                }
            }
        },200);
    });
}else{
    $('#step2 .mc-link, #step4 .mc-link').addClass('white');
}

var step1Slider = $('#page-home #step1 .bg-slider');

step1Slider.owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    dots: false,
    items: 1,
    rewind: true,
    autoplay: true,
    mouseDrag: false,
    autoplayHoverPause: false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplayTimeout: 6000
});

step1Slider.on('changed.owl.carousel', function(event) {
    var id = event.item.index + 1;
    $('[data-slider-page]').removeClass('active');
    $('[data-slider-page="'+id+'"]').addClass('active');
});

$(document).delegate('[data-slider-page]', 'mouseenter', function(){
    var id = $(this).attr('data-slider-page') - 1;
    step1Slider.trigger('to.owl.carousel', [id, 500]);
    $('[data-slider-page]').removeClass('active');
    $(this).addClass('active');

    step1Slider.data('owl.carousel').options.autoplay = false;
    step1Slider.trigger('refresh.owl.carousel');
});

$(document).delegate('[data-slider-page]', 'click', function(){
    var url = $(this).attr('data-slider-url');
    if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
        window.location = url;
    }
});

$(document).delegate('[data-slider-page]', 'mouseleave', function(){
    step1Slider.data('owl.carousel').options.autoplay = true;
    step1Slider.trigger('refresh.owl.carousel');
});

var news = $('#page-home .news .owl-carousel');

news.owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    items: 1,
    rewind: true,
    autoplay: true,
    autoplayHoverPause: true,
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    smartSpeed: 450,
    navText: ['<i class="mc-prev"></i>','<i class="mc-next"></i>']
});

if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    google.maps.event.addDomListener(window, 'load', init);
    function init()
    {
        var mapOptions = {
            zoom : 16,
            styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}],
            scrollwheel : false,
            center : new google.maps.LatLng(41.023407, 29.129789),
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
    }
}