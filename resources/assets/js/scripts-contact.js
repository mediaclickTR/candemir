
google.maps.event.addDomListener(window, 'load', init);
function init()
{
    var mapOptions = {
        zoom : 17,
        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}],
        scrollwheel : true,
        center : new google.maps.LatLng(41.006878, 29.071819),
    };
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
}

if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    $(document).ready(function(){
        title_canvas();
        $('.footer').addClass('disabled');
        $('.title-canvas').removeClass('disabled');
    });
    function title_canvas(){
        var item = $('.follow-canvas');
        $('.header .left-bar .title-canvas').css({
            'top': item.offset().top - $(window).scrollTop(),
            'height': item.height()
        });
    }
}else{
    $('.footer').addClass('disabled');
}

$(document).ready(function(){
    ahovalidator.build('#page-contact .top-head .area form');
});