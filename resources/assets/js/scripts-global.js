
$(document).ready(function(){
    if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent) || $(window).width() >= 1024) {
        $('.header .menu-bars').click();
    }
    if($('#mpadminbar').length){
        $('body').attr('style', 'margin-top: 0 !important;');
        if ($('#page-home').length) {
            $('body').attr('style', 'overflow: hidden; margin-top: 0 !important;');
        }
        $('.header').attr('style','top: 32px');
    }

    var sld = $('#page-services-security .images, #page-services-detail .images');
    sld.owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 2,
        rewind: true,
        autoplay: true,
        autoplayHoverPause: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        responsive:{
            600:{
                items: 4
            }
        }
    });

    var sld = $('#page-services-security .slide, #page-services-detail .slide');
    sld.owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 1,
        rewind: true,
        autoplay: true,
        autoplayHoverPause: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    });

});

var stepsPage = 1;

$('.header .menu-bars').click(function(){
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.header .menus').removeClass('active');
    }else{
        $(this).addClass('active');
        $('.header .menus').addClass('active');
    }
});

$('.header .menus ul li').hover(function(){
    $(this).parent().addClass('hovered');
},function(){
    $(this).parent().removeClass('hovered');
});

if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    $('.header .menus ul li').click(function(){
        $('.header .menus ul li ul').hide();
        if ($(this).find('ul').length) {
            $(this).find('ul').show();
        }
    });
}

var iScrollPos = 0;

$(window).scroll(function () {
    var head = $('.header');
    var iCurScrollPos = $(this).scrollTop();
    iScrollPos = iCurScrollPos;
    if($(document).scrollTop()>0){
        head.addClass('scroll');
    }else{
        head.removeClass('scroll');
    }
});

function imgLazzyLoad(){
    $('*[data-src]').each(function(){
        var _this = $(this);
        var src = $(_this).attr('data-src');
        if ( $(_this).offset().top <= ($(window).scrollTop() + $(window).height()) ) {
            $(_this).attr('src', src);
            $(_this).removeAttr('data-src');
        }
    });
}

$(window).scroll(function () {
    imgLazzyLoad();
});

$(window).on('load', function(){
    imgLazzyLoad();
});
